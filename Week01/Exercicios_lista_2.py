# -----------------------------------------------
# --------- Andreia Faria -----------------------
# -----------------------------------------------
# -- Exercicios lista 2 -------------------------
# -----------------------------------------------
# --- 1 -----------------------------------------
# -----------------------------------------------


a = 3
b = 5
c = a*b, a+b, a/b
print (c)


# -----------------------------------------------
# --- 2 -----------------------------------------
# -----------------------------------------------


lista = ['1.Pão Alentejano', '2.Bolo Lêvedo(dos Açores)', '3.Bolo do Caco (da Ilha da Madeira)', '4.Broa', '5.I want to leave' ]
print (*lista, sep = '\n')

while True:
    print ('Please, choose a number:')
    num = int(input())
    if num == 1:
        print ('Pão Alentejano')
        continue
    elif num == 2:
        print ('Bolo Lêvedo(dos Açores)')
        continue
    elif num == 3:
        print ('Bolo do Caco (da Ilha da Madeira)')
        continue
    elif num == 4:
        print ('Broa')
        continue
    elif num > 5:
        print ('Not in the menu.Sorry.')  
    elif num == 5:
        print ('I want to leave')
        break


# -----------------------------------------------
# --- 3 -----------------------------------------
# -----------------------------------------------


x = 5 + ord('A') # ord A = 65
y = 0
while True:
    y = (x%2) + 10*y  # y = 0
    x = x // 2  # x = 35
    print ('x =', x, 'y =', y)
    if x == 0:
        break  # o resultado final de cada passagem pelo loop volta a ser calculado até o x ser 0

while y != 0:
    x = y % 100 # x = 1
    y = y // 10 # y = 11000 (//-arredonda para um num inteiro)
    print ('x =', x, 'y =', y) # cada passagem no loop volta a ser calculada até y ser 0


# -----------------------------------------------
# --- 4 -----------------------------------------
# -----------------------------------------------


for n in range(1, 6):
    print (n*' ', n)