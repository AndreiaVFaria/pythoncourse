# -----------------------------------------------
# --------- Andreia Faria ------ aula 4 ---------
# -----------------------------------------------
# -- pag 07 -------------------------------------
# -----------------------------------------------


def thing():
   print('Hello')
   print('Fun')
thing()
print('Zip')
thing()


# -----------------------------------------------
# -- pag 09 -------------------------------------
# -----------------------------------------------
# -- building functions -------------------------
# -----------------------------------------------


def music_lyrics():
   print("We are here")
   print("We are here for all of us") 
# defines the function but doesn't execute the body
   
   
# -----------------------------------------------
# -- pag 11 -------------------------------------
# -----------------------------------------------


def food (vegetable):
    if vegetable == 'tomato':
        print ('I bought tomato')
    elif vegetable == 'orange':
        print ('I bought orange')
    else:
        print ('i bought other vegetables') # mesmo sem o else ele funciona

food ('orange')


# -----------------------------------------------
# -- pag 12 -------------------------------------
# -----------------------------------------------


def greet():
    return 'Hello'
print (greet(), 'Anabela')
print (greet(), 'Rodrigo')


# -----------------------------------------------
# -- pag 14 -------------------------------------
# -----------------------------------------------
# -- Multiple Parameters / Arguments ------------
# -----------------------------------------------


def addthree(a, b, c):
    added = a + b + c
    subtration = a - b - c
    multiplication = a * c
    return added, subtration, multiplication

x = addthree (3, 5, 1)
print (x[0])
print (x[2])


# -----------------------------------------------
# -- pag 15 -------------------------------------
# -----------------------------------------------
# -- Global and local Variables -----------------
# -----------------------------------------------


def food ():
    eggs = 'food local'
    print (eggs) #print 'food local'

def more_food ():
    eggs = 'more_food local'
    print (eggs) # print more_food local'
    food ()
    print (eggs)

eggs = 'global'
more_food ()
print (eggs) # prints 'global'


# -----------------------------------------------
# -- pag 16 -------------------------------------
# -----------------------------------------------
# -- Assignments operators ----------------------
# -----------------------------------------------


a = 21 b = 10 c = 0 c=a+b
c += a
c *= a
c /= a 
c = 2
c %= a 
c **= a 
c //= a


# -----------------------------------------------
# -- pag 20 -------------------------------------
# -- ex 1 ---------------------------------------
# -----------------------------------------------


def even_odd (n):
      for i in range(n, -1,-1):
          if i %2 == 1:
              print ('This is a odd number:', i)
          else:
              print ('This is a even number:', i)

n = (int(input("Insert number:"))) 
even_odd (n)


# -----------------------------------------------
# -- pag 21 -------------------------------------
# -- ex 2 ---------------------------------------
# -----------------------------------------------








