# -----------------------------------------------
# --------- Andreia Faria ------ aula 1 ---------
# -----------------------------------------------
# -- pag 40 -------------------------------------
# -- ex 1 ---------------------------------------
# -- a. -----------------------------------------
# -----------------------------------------------


print("Welcome to python course")
print('Welcome to python course')


# -----------------------------------------------
# -- pag 41 -------------------------------------
# -- b. -----------------------------------------
# -----------------------------------------------
 

print(8+5*13)


# -----------------------------------------------
# -- pag 41 -------------------------------------
# -- c. -----------------------------------------
# -----------------------------------------------


print("Python is fun", 31)
print("Hello" + "Andreia Faria")
print("Hello" , "Andreia Faria")
print("Hello Andreia Faria")
print("Hello" + "\t Andreia Faria" + "\t You are splitting \t spaces \n and Lines")
name = "Andreia Faria"
print (name)
print ("Hello" , name)
age = 31 # essa variavel armazena a idade
print (age)
greeting = "Hello "
print (greeting + name)
print (greeting , name , age)
# print (greeting + age)
print ("Hello " * 5)
print (greeting * 10)


# -----------------------------------------------
# -- pag 44 -------------------------------------
# -----------------------------------------------


print ('%s is %d years old' % (name, age))
print ('{} is {} years old' .format(name, age)) #forma usual do python
print (f'{name} is {age} years old') #forma mais facil/util e a mais usada


# -----------------------------------------------
# -- pag 46 -------------------------------------
# -----------------------------------------------

'''
s1 = "Andreia"
s2 = "Beatriz"

print("Student1\t",s1)
'''

print("Student1\tAndreia") 
print("Student2\tBeatriz")


# -----------------------------------------------
# -- pag 48 -------------------------------------
# -- ex 3 ---------------------------------------
# -----------------------------------------------


print ("Please, enter your name:")
student_name = input() #com esta funcao recebemos dados do usuario\externos
print ("Welcome,", student_name)


# -----------------------------------------------
# -- pag 49 -------------------------------------
# -- ex 4 ---------------------------------------
# -----------------------------------------------


name = input("John\nAnna\nJoe").split()
print(name)


str1, str2, str3 = input("John Anna Joe").split()
print(str1, str2, str3)


# -----------------------------------------------
# -- pag 51 -------------------------------------
# -- ex 5 ---------------------------------------
# -----------------------------------------------


quantity = 3
totalMoney = 1000
price = 450
statement1 = "I have {1} dollars so I can buy {0} football for {2:.2f} dollars."
print(statement1.format(quantity, totalMoney, price))


# -----------------------------------------------
# -- pag 54 -------------------------------------
# -- ex 6\7 -------------------------------------
# -----------------------------------------------


frase= "Lisbon is in Portugal" # 23 letras

#print (frase)
#print (frase[0])


print (frase[1]) #a contagem começa do zero
print (frase[18])
print (frase[14])
print (frase[20])

print (frase[0:6])

cidade = (frase[0:6])
print (cidade)

print (frase[10:14])
print (frase[10:])


print (frase[:6])

print (frase[:6] + frase[6:]) #bom para separar graficamente texto

print (frase[:]) #quando nao se indica numero ele assume a totalidade



#--------------------------------------------
#input:
# nome
# idade
#output:
# <nome e idade>
# exemplo de comentários para códigos elaborados
#--------------------------------------------

# frase = -109876543219876543210
# frase = lisbon is in portugal

print (frase [-21:-15])
print (frase [-8:-5])
print (frase [-14:-10]) #o ultimo num não é incluido


# -----------------------------------------------
# -- pag 40 -------------------------------------
# -- ex 1 ---------------------------------------
# -----------------------------------------------


a = 12
b = 3

#print (type(a)) #especifica qual é a variavel, neste caso é um numero inteiro
#print (type(frase)) #neste caso diz que é uma string

#print (a / b)
#print (a // b) #as duas barras arredonda para um numero int

print(a + b)
print(a - b)
print(a * b)
print(a / b)
print(a // b)
print(a % b)


# -----------------------------------------------
# -- pag 60 -------------------------------------
# -- ex 9 ---------------------------------------
# -----------------------------------------------


num1=int(input("Enter first number:"))
num2=int(input("Enter second number:"))
sum = num1 + num2
print (str(num1) + "+" + str(num2) + "=",sum )


# -----------------------------------------------
# -- pag 62 -------------------------------------
# -- ex 10 --------------------------------------
# -----------------------------------------------


credit_str = "0000----0000----5555"
new_credit_str = credit_str[credit_str.index('5'):credit_str.index('5') + len('5555')]
print(new_credit_str)


# -----------------------------------------------


# import sys
# print("\nInteger value information: ", sys.int_info)
# print("\nMaximum size of an integer: ", sys.maxsize)

