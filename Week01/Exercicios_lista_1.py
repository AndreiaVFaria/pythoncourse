# -----------------------------------------------
# --------- Andreia Faria -----------------------
# -----------------------------------------------
# -- Exercicios lista 1 -------------------------
# -----------------------------------------------
# --- 1 -----------------------------------------
# -----------------------------------------------


# ------------------------- #
# --- a) integers / int --- #
# b) floating point / float #
# c) floating point / float #
# --- d) strings / str ---- #
# ------------------------- #

age = 31
print (type(age))
yard_square_meters = (10.2*12)
print (type(yard_square_meters))
bank_acount = 247.58
print (type(bank_acount))
song = "Energia racional"
print (type(song))


# -----------------------------------------------
# --- 2 -----------------------------------------
# -----------------------------------------------


print ("Enter a word please:")
str1 = int(input()


# -----------------------------------------------
# --- 3 -----------------------------------------
# -----------------------------------------------


s = "abc"
print (len(s))
print (s[0]*3 + s[1]*3 + s[2]*3)


# -----------------------------------------------
# --- 4 -----------------------------------------
# -----------------------------------------------


s = 'aaabbbccc'
sub1 = 'b'
sub2 = 'ccc'

print (s.find(sub1))
print (s.find(sub2))
x = s.replace('a', 'X')
ax = s.replace('a', 'X', 1)
print (x)
print (ax)


# -----------------------------------------------
# --- 5 -----------------------------------------
# -----------------------------------------------


txt = 'aaa bbb ccc'
print (txt.upper())
print (txt.replace('a', 'A').replace('c', 'C'))


# -----------------------------------------------
# --- 6 -----------------------------------------
# -----------------------------------------------


a = 10
b = a # b = 10
c = 9
d = c # d = 9
c2 = c + 1 # valor final do c é 10

print (a)
print (b)
print (c2)
print (c)
print (d)


# -----------------------------------------------
# --- 7 -----------------------------------------
# -----------------------------------------------


x = 2
y = 10
x, y = y, x

print (x)
print (y)


# x, y = 2, 10 # tb podemos fazer em conjunto


# -----------------------------------------------
# --- 8 -----------------------------------------
# -----------------------------------------------

# -------
# doesn't work, 'if/else' need to be organize
# -------

#print (" Enter a number :")
#a = int ( input ())
#if a % 2 == 0 and a < 100:
# print ("The number is even and smaller than 100")
#else :
# if a >= 100:
# print ("The number is even and equal or higher than 100")
#if a % 2 != 0 and a < 100:
# print ("The number is odd and smaller than 100")
#else :
# if a >= 100:
# print ("The number is odd and equal or higher than 100")

# -------
# suggestion
# -------

print ('Enter a number:')
a = int(input())
if a%2 == 0 and a < 100:
    print ('The number is even and smaller than 100.')
elif a%2 == 0 and a >= 100:
    print ('The number is even and equal or higher than 100')
elif a%2 != 0 and a < 100:
    print ('The number is odd and smaller than 100')
else:
    print ('The number is odd and equal or higher than 100')