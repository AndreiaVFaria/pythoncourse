# -----------------------------------------------
# --------- Andreia Faria ------ aula 2 ---------
# -----------------------------------------------
# -- pag 07 -------------------------------------
# -- 1. -----------------------------------------
# -----------------------------------------------


a = 3
b = 2
print ((a / b)) #divisão
print ((a // b)) #divide e arredonda para baixo
print ((a % b)) #sobra da divisão
print ((a * b)) #multiplicação


# -----------------------------------------------
# -- 2. -----------------------------------------
# -- average calculation ------------------------
# -----------------------------------------------


a = [2, 4]
b = [4, 8, 9]
c = [12, 14/16, 15]
average1 = sum(a) / len(a)
average2 = sum(b) / len(b)
average3 = sum(c) / len(c)
print (round(average1))
print (round(average2))
print (round(average3, 2))


# -----------------------------------------------
# -- 3. -----------------------------------------
# -- sphere volume calculation ------------------
# -----------------------------------------------


def volume_sphere(raio): # recebe o parametro descrito na ultima linha
    pi = 3.1415
    const = 4/3
    power = 3
    volume = (const*pi*(raio**power))
    print (f"{volume:.3f} U")
raio = float(input ("Please, enter radious:"))
volume_sphere(raio)


import math # o resultado é diferente pq o pi do math tem mais casa decimais
def volume_sphere(raio): # recebe o parametro descrito na ultima linha
    const = 4/3
    power = 3
    volume = const*math.pi*(raio**power)
    print (f"{volume:.3f} U")
raio = float(input ("Please, enter radious:"))
volume_sphere(raio)


import math
def volume_sphere(raio, power, const): # recebe o parametro descrito na ultima linha
    volume = const*math.pi*(raio**power)
    print (f"{volume:.3f} U")
raio = float(input ("Please, enter radious:"))
const = 1
power = 3
volume_sphere(raio, power, 4/3)


# -----------------------------------------------
# -- 4. -----------------------------------------
# -- even or odd numbers ------------------------
# -----------------------------------------------


for i in (1, 5, 20, 60/7):
    b = i%2
    if b == 0:
        print (i, 'Even number.')
    else:
        print (i, 'Odd number.')


# -----------------------------------------------
# -- pag 12 -------------------------------------
# -- Comparisons using print --------------------
# -----------------------------------------------


print(4==5)
print(6>7)
print(15<100)
print('hello' == 'hello')
print('hello' == 'Hello')
print('dog' != 'cat')
print( True == True)
print(True != False)
print( 42 == 42.0)
print( 42 == '42’)
print('apple' == 'Apple')
print('apple' > 'Apple') #List of Unicode characters
print('A unicode is', ord(‘A’) ,’ ,a unicode is’ , ord(‘a’)) #List of Unicode characters


# -----------------------------------------------
# -- pag 14 -------------------------------------
# -----------------------------------------------


minimum_age = 18
age = 31
if age > minimum_age:
    print('Congrats, you can get your license') 
else:
    print('Please, come back in {0} years' .format(minimum_age - age))


# -----------------------------------------------
# -- pag 15 -------------------------------------
# -----------------------------------------------


user_name = 'Josi'
password = 'swordfish'

if user_name == 'Josi':
    print('Hello Josi.')
if password == 'swordfish':
    print('Access granted.')
else:
    print('Wrong password!')


# -----------------------------------------------
# -- pag 16 -------------------------------------
# -----------------------------------------------


minimum_age = 18
user_age = 15

if user_age < minimum_age:
    print ('Please come back in {0} years'.format(minimum_age - user_age))
elif user_age > 75:
    print ('Probably, you can not drive anymore. Please, make an appointment with your doctor.')
else:
    print ('Congrats, you can get your license.')


# -----------------------------------------------
# -- pag 18 -------------------------------------
# -- ex 1 ---------------------------------------
# -----------------------------------------------
# -- guess number -------------------------------
# -----------------------------------------------


guess_number = 5
user_number = int(input("Please, enter a number:")) # precisa do int para nao assumir como uma string

if user_number < guess_number:
    print ("Please, guess higher.")
elif user_number == guess_number: # podemos usar as elif que quisermos
    print ("Congrat, you found de answer.")
else:
    print ("Please, guess lower.")


while True:
    guess_number = 5
    user_number = int(input("Please, enter a number from 0 to 10:"))
    if user_number < guess_number:
        print ('Please guess higher.')
        continue
    elif user_number > guess_number:
        print ('Please guess lower.')
        continue
    else:
        print ('Congrats, you found the answer.')
        print ('Good job :)')
        break


# -----------------------------------------------
# -- pag 20 -------------------------------------
# -- Comparisons using print --------------------
# -----------------------------------------------


print ((4 < 5) and (5 < 6))
print ((4 < 5) and (9 < 6)) # se nao tiver o segundo parentesis ele executa a primeira info
print ((1 == 2) or (2 == 2)) # com o or se um é verdadeiro independentemente da ordem vai dar verdadeiro, ver tabela aula2 pag19
print (2 + 2 == 4 and not 2 + 2 == 5) # o and faz true(porque analisa a primeira info) mas o not torna o true em false
print ((2 + 2 == 4 and not 2 + 2 == 5) and 2 * 2 == 2 + 2)



# -----------------------------------------------
# -- pag 22 -------------------------------------
# -- ex 2 ---------------------------------------
# -----------------------------------------------


your_age = int(input("How old are you?"))
minimun_age = 18
retire_age = 68
age = ("{0} years old." .format(your_age))

if your_age <= 18:
    print (age, "\nYou are too young to work, come back to school.")
elif 18 < your_age < 68:
    print (age, "\nHave a good day at work.")
else:
    print (age, "\nYou have worked enough, Let’s Travel now")


# -----------------------------------------------
# -- pag 23 -------------------------------------
# -- Code using conditionals --------------------
# -----------------------------------------------


a = 4
b = 2

if a==5 and b>0:
    print ('a is 5 and',b,'is greater than zero.')
else:
    print ('a is not 5 or',b,'is not greater than zero')


# -----------------------------------------------
# -- pag 24 -------------------------------------
# -- Be careful using conditionals --------------
# -----------------------------------------------


day = "Saturday"
temperature = 24
raining = True

if day == "Saturday" and temperature > 20 or raining:
    print ("Go out ")
else:
    print ("Better finishing python programming exercises")


# -----------------------------------------------
# -- pag 25 -------------------------------------
# -- Repeated Steps -----------------------------
# -----------------------------------------------


n = 5
while n > 0:
    print (n)
    n = n - 1
print("Boom!!")
print (n)


# -----------------------------------------------
# -- pag 26 -------------------------------------
# -- An Infinite Loop ---------------------------
# -----------------------------------------------


n = 3
while n > 0:
    print ("Time")
    print ("ticking ")
print ("Stopped")


# -----------------------------------------------
# -- pag 28 -------------------------------------
# -- Breaking Out of a Loop ---------------------
# -----------------------------------------------


while True:
    line = input('> ')  
    if line == 'done' :
        break
        print (line)
print('Done!')


# -----------------------------------------------
# -- pag 31 -------------------------------------
# -- Finishing an Iteration with continue -------
# -----------------------------------------------


while True:
    line = input('> ')
    if line[0] == '#' : # o [0] interpreta a primeira letra da linha, ou seja desde que nao tenha um # vai fazer print
        continue
    if line == 'done' :
        break
        print(line)
print('Done!')
