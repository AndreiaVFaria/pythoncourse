# --------- Andreia Faria -----------------------
# -----------------------------------------------
# -- Exercicios lista 3 -------------------------
# -----------------------------------------------
# --- 1 -----------------------------------------
# -----------------------------------------------


(5 > 4) and (3 == 5) # False ('and' truth table) 
not (5 > 4) # False, o not anula a sua veracidade ('not' truth table) 
(5 > 4) or (3 == 5) # True ('or' truth table) 
not ((5 > 4) or (3 == 5)) # False
(True and True) and (True == False) # False
(not False) or (not True) # True, o not inverte os papeis


# -----------------------------------------------
# --- 2 -----------------------------------------
# -----------------------------------------------

 
spam = 10
if spam == 10: # Code Block, if - else statement 
   print ('eggs') 
   if spam > 5: # Code Block 
     print ('bacon') 
   else: # Code Block 
     print ('ham') 
   print ('spam')
print ('spam')



# -----------------------------------------------
# --- 3 -----------------------------------------
# -----------------------------------------------


r1 = '1.[0, 25]:'
r2 = '2.[26, 50]:'
r3 = '3.[51, 75]:'
r4 = '4.[76, 100]:'

n = 10
y = 0
if n >= 0 and n <= 25:
    y = y+1
    print (r1, y)
    if n >= 26 and n <= 50:
        print (r2, y)
    if n >= 51 and n <= 75:
        print (r3, y)
    if n >= 76 and n <= 100:
        print (r4, y)


l = [2, 61, -1, 0, 88, 55, 3, 121, 25, 75]
a = 0
b = 0
c = 0
d = 0

for i in l:
    if i >= 0 and i <= 25:
        a = a+1
    if i >= 26 and i <= 50:
        b = b+1
    if i >= 51 and i <= 75:
        c = c+1
    if i >= 76 and i <= 100:
        d = d+1

print (r1, a)
print (r2, b)
print (r3, c)
print (r4, d)


# -----------------------------------------------
# --- 4 -----------------------------------------
# -----------------------------------------------


txt = 'Catching up Start with The Portuguese: The Land and Its People (3) by Marion Kaplan (Penguin), a one-volume introduction ranging from geography and history to wine and poetry, and Portugal: A Companion History (4) by Jos� H Saraiva (Carcanet Press), a bestselling writer and popular broadcaster in his own country.'

print (txt.isalnum()) 
print (txt.isupper())
print (txt.count('P'))
print (txt.find('M'))
print (txt.isprintable())
print (len(txt))
print (txt.isspace())
print (txt.isalpha())
print (max(txt))
print (min(txt))


# -----------------------------------------------
# --- 5 -----------------------------------------
# -----------------------------------------------

for i in range (0, 16):
    print (i, bin(i))

