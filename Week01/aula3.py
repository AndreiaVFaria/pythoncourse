# -----------------------------------------------
# --------- Andreia Faria ------ aula 3 ---------
# -----------------------------------------------
# --- Debugger ----------------------------------

a = 10
b = a
c = 9
d = c
c = c + 1

# debugger - assinalar a linha - debug file - run current line

n = 5
while n > 0:
    print(n)
    n = n - 1
print("Boom!!")
print(n)


# -----------------------------------------------
# -- pag 05 -------------------------------------
# -- loop in word -------------------------------
# -----------------------------------------------


for student_age in [5, 4, 3, 2, 1, 0] :
  print (student_age)
print ("Boommm!!")


friends = ["Ana", "Filipe", "João"]
for friends in friends :
  print ("Congrats on your new job:", friends)
print ("Done")


for i in [5, 4, 3, 2, 1] :
  print(i)


# -----------------------------------------------
# -- pag 08 -------------------------------------
# -- loop range word ----------------------------
# -----------------------------------------------


print ("Before")
for i in range(10):  
  print (i)
print ("After")


print ("Before")
for i in range(2, 6):
  print (i)
print ("After")


print ("Before")
for i in range(15, 0, -5):
  print (i)
print ("After")


# -----------------------------------------------
# -- pag 11 -------------------------------------
# -----------------------------------------------


print ("Before")
for things in [9, 41, 12, 3, 74, 15]:
  print (things)
print ("After")


# -----------------------------------------------
# -- pag 13 -------------------------------------
# -- ex 1 ---------------------------------------
# -----------------------------------------------


# Input - The Best of made in Potugal - Hat, Soaps, Shoes, Tiles & Ceramics, Cork
# Output - TBPHSSTCC

msg = "The Best of made in Potugal - Hat, Soaps, Shoes, Tiles & Ceramics, Cork"


for ch in msg:
   if ch >= "A" and ch <= "Z" :
      print(ch, end="")


# -----------------------------------------------
# -- pag 14 -------------------------------------
# -- ex 2 ---------------------------------------
# -----------------------------------------------


print ("Before")
for i in range(0, 50, 4):
  print (i)
print ("After")


# -----------------------------------------------
# -- pag 15 -------------------------------------
# -- loop largest number ------------------------
# -----------------------------------------------


largest_so_far = 1

print("Before", largest_so_far)
for the_num in [9, 41, 12, 3, 74, 15]:
    if the_num > largest_so_far:
        largest_so_far = the_num
    print(largest_so_far, the_num)

print("After", largest_so_far)


# -----------------------------------------------
# -- pag 16 -------------------------------------
# -- loop to count ------------------------------
# -----------------------------------------------


loop_interaction = 0
print ('Before', loop_interaction)
for things in [9, 41, 12, 3, 74, 15]:
    loop_interaction = loop_interaction + 1
    print(loop_interaction + 1)
print('After', loop_interaction)


# -----------------------------------------------
# -- pag 17 -------------------------------------
# -- loop to sum --------------------------------
# -----------------------------------------------


sum = 0
print('Before', sum)
for thing in [9, 41, 12, 3, 74, 15] :
   sum = sum + thing
   print(sum, thing)
print('After', sum)


# -----------------------------------------------
# -- pag 18 -------------------------------------
# -- loop to find the average value -------------
# -----------------------------------------------


count = 0
sum = 0
print('Before', count, sum)
for value in [9, 41, 12, 3, 74, 15] :
   count = count + 1
   sum = sum + value
   print(count, sum, value)
average_value = sum / count
print('After', count, sum,
average_value)


# -----------------------------------------------
# -- pag 19 -------------------------------------
# -- loop to select values ----------------------
# -----------------------------------------------


print('Before')
for value in [9, 41, 12, 3, 74, 15] :
   if value > 20:
       print('Large number',value)
print('After')


# -----------------------------------------------
# -- pag 20 -------------------------------------
# -----------------------------------------------
# -- loop to search using boolean ---------------
# -----------------------------------------------


found = False
print ('Before', found)
for value in [9, 41, 12, 3, 74, 15]:
    if value == 3:
       found = True 
       print(found, value)
    if value != 3:
       found = False
       print(found, value)
print ('After', found)


# -----------------------------------------------
# -- pag 21 -------------------------------------
# -----------------------------------------------
# -- loop to find the smallest value ------------
# -----------------------------------------------
# -----------------------------------------------


smallest = None
print('Before')
for value in [9, 41, 12, 3, 74, 15] :
   if smallest is None :
       smallest = value
   elif value < smallest :
       smallest = value
   print(smallest, value)
print('After', smallest)