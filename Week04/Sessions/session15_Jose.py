# -----------------------------------------------
# --------- Andreia Faria ----- Session 15 ------
# -----------------------------------------------
# -- 02.11.2020 ---------------------------------
# -----------------------------------------------
# -- pag 13 -------------------------------------
# -----------------------------------------------
# -- Tuples revision ----------------------------
# -----------------------------------------------


t = ('Rose', 'Sunflower', 'Tulip')
print (t[2])
print (t.index('Sunflower'))
# trabalhar com o index para printar ou recolher info do nosso tuple

t = (1, 4, 7, 4, 2, 3, 4)
print (t.count(4))
# conta o número de ocurrencias no tuple
print (max(t))
# indica o valor mais alto 
print (t + t)


print ((8,9) == (9,8))
# podemos trabalhar em True e False com o tuple

t = (6, 9, 3, 5, 1)
x = list(t)
# precisamos de converter para uma lista para .remove(), .append() ...
x.remove(3)
print (tuple(x))

z = ((6, 9, 7), (3, 5, 6), (0, 3, 4))
u = zip(*z)
print (list(u))

a = (1, 2, 3)
b = (4, 5, 6)
c = zip(a,b)
print (list(c))


# -----------------------------------------------
# -- pag 14 -------------------------------------
# -----------------------------------------------
# -- Dictionary revision ------------------------
# -----------------------------------------------
'''
.get(), .pop(), .popitem(), .copy(), .update()
tb podemos usar funções do python para manipular dicionários
sum()
'''

d = {'brand':'cherry', 'model':'arizo', 'color':'white'}
d['color'] = 'green'
print(d)
x = d.get('brand')
print(x)

print(list(d.keys()))
print(list(d.values()))

print(d.pop('color'))
# têm comportamentos diferentes
d.pop('model')
print(d)


num = {'ali':[12, 13, 8], 'sara':[15, 6, 18], 'taha':[5, 18, 12]}
d = {k : sorted(v) for k, v in num.items()}
print(d)


k = ['red', 'green']
v = ['#FF0000', '#008000']
z = zip(k, v)
d = dict(z)
print(d)

x = zip(v, k)
c = dict(x)
print(c)


# -----------------------------------------------
# -- pag 15 -------------------------------------
# -----------------------------------------------
# -- Set revision -------------------------------
# -----------------------------------------------


fruits = {'apple', 'orange', 'peach'}
fruits.add('cherry')
# .add() só leva um argumento
print(fruits)

fruits.update(['mango', 'grapes', 'lemon'])
print(fruits)
fruits.remove('orange')
# .remove() só leva um argumento
print(fruits)


X = {1, 2, 3}
Y = {2, 3, 4}
print(X|Y) # (X.union(Y))
print(X&Y) # (X.intersection(Y))

A = {1, 2, 4}
B = {1, 2, 3, 4, 5}
print(A.issubset(B)) # True
print(B.issubset(A)) # False


# -----------------------------------------------
# -- pag 16 -------------------------------------
# -----------------------------------------------
# -- Files --------------------------------------
# -----------------------------------------------


# files open

epopeia = open ('C:\\Users\\Huga\\Desktop\\pythoncourse\\Week04\\epopeia.txt', 'r')

for line in epopeia:
    print (line)
    if 'tempestade' in line.lower():
        print (line)
epopeia.close()
# os caracteres especiais não são lidos correctamente


with open ('C:\\Users\\Huga\\Desktop\\pythoncourse\\Week04\\epopeia.txt', 'r') as epopeia:
# loop, lê cada linha e procura
    for line in epopeia:
        if 'tempestade' in line.lower():
            print (line)
# com o with open o arquivo fecha automaticamente,
# para abertura de arquivos a melhor prático é o with open            
print (line)


# -----------------------------------------------
# -- pag 17 -------------------------------------
# -----------------------------------------------


encoding_type = 'utf-8'
# encoding é um atributo
# com o encoding 'utf-8' caracteres especiais já são lidos correctamente
# existem vários tipos de encoding

file_path = 'C:\\Users\\admin\\Desktop\\pythoncourse\\Week04\\epopeia.txt'
with open (file_path, 'r', encoding = encoding_type) as epopeia:
    line = epopeia.readline()
    # faz uma verificação antes do loop (mais facil para resolução de problemas)
    # e define o line do loop while
    while line:
        print(line, end='')
        line = epopeia.readline()
        

file_path = 'C:\\Users\\Huga\\Desktop\\PythonCourse\\Week04\\epopeia.txt'
with open (file_path, 'r', encoding ='utf-8') as epopeia:
    poem = epopeia.readlines()
    # cria um lista de strings
    print(type(poem))
print (poem)


file_path = 'C:\\Users\\Huga\\Desktop\\PythonCourse\\Week04\\epopeia.txt'
with open (file_path, 'r', encoding ='utf-8') as epopeia:
    text = epopeia.read()
    # cria uma string gigante, 
    # o que dificulta se quisermos encontrar algo especifico
    print(type(text))
print (text)


for linea in poem [::-1]:
    print (linea, end='')
print (type(poem))
# class - list    
# por ser uma lista o print é linha a linha por ordem inversa


for linea in text[::-1]:
    print (linea, end='')
print (type(text))   
# class - str
# por ser uma string o print é caracter a caracter por ordem inversa


# -----------------------------------------------
# -- pag 18 -------------------------------------
# -----------------------------------------------


'''
criação do doc .json

{
"data": [{ 
 	"id": "12341",
 	"name" : " Andreia Faria",
 	"address" : {
		"city" : "Lisboa",
		"state" : "Lisboa",
		"postal_code" : "1800106"
		},
 	"position" : "Python Student"
		}
 	]
}
'''
# jsonlint.com --- recurso para ver se o json está bem escrito


# -----------------------------------------------
# -- pag 19 -------------------------------------
# -----------------------------------------------


import json
with open ('C:\\Users\\admin\\Desktop\\pythoncourse\\Week04\\data.json') as json_file:
    jsonObject = json.load(json_file)
    # com json usamos o load em vez do read do txt
    name = jsonObject ['data'][1]['name']   
    address = jsonObject ['data'][1]['address']['city'] 
    # se o index for 0 ele encontra o primeiro objecto do que queremos procurar
    # print(jsonObject)
    print(name)
    print(address)
    # o json funciona por hierarquia ao contrario dos txt
    a = json.dumps(jsonObject, indent=4)
    print (a)

print(jsonObject ['data'][1]['id'])


# -----------------------------------------------
# -- pag 20 -------------------------------------
# -----------------------------------------------


# pandas --- é frequentemente usado para manipular ficheiros excel
import pandas as pd
a = 'C:\\Users\\Huga\\Desktop\\PythonCourse\\Week04\\Employee_data.xlsx'
all_data = pd.read_excel(a, index_col=1)

email = all_data['email'].head()
company_name = all_data['company_name']
xl = pd.ExcelFile(a)
df = xl.parse("b")


# -----------------------------------------------
# -- pag 21 -------------------------------------
# -----------------------------------------------


import json
file_path = 'C:\\Users\\Huga\\Desktop\\PythonCourse\\Week04\\data.json'

try:
    with open(file_path) as json_file:
        json_object = json.load(json_file)
except:
    print('File not found.')
else:
    name = json_object ['data'][1]['name']   
    address = json_object ['data'][1]['address']['city'] 
    print(json_object)
    print(name)
    print(address)    
finally:
    print ('Something happened.')      


# -----------------------------------------------
# -- pag 22 -------------------------------------
# -----------------------------------------------
# -- Lambda expressions -------------------------
# -----------------------------------------------


def power_tree(x):
    return(x**3)
print (power_tree(10))

a = lambda x : x**3
print (a(10))
a = lambda b, c : b*c
print (a(2, 4))

def larger_num(n1, n2):
    if n1 > n2:
        return n1
    else:
        return n2
print (larger_num(9, 3))

larger_num = lambda n1, n2: n1 if n1 > n2 else n2
print (larger_num(9, 3))

# A lambda function is a small anonymous function.
# A lambda function can take any number of arguments, 
# but can only have one expression.


# -----------------------------------------------
# -- pag 23 -------------------------------------
# -----------------------------------------------
# -- Maps ---------------------------------------
# -----------------------------------------------


text = 'SMALL ANONYMOUS FUNCTION   '

def char_lowercase():
    char_lowercase = [char.lower() for char in text]
    return char_lowercase
print (char_lowercase())

# with map
def map_lowercase():
    map_lowercase = list(map(str.lower, text))
    return map_lowercase
print (map_lowercase())

def comp_words():
    words_lowercase = [word.lower() for word in text.split(' ')]
    return words_lowercase
print (comp_words())

def map_words():
    map_words = list(map(str.lower, text.split(' ')))
    return map_words
print (map_words())
# isto é bom, fica mais compacto


# -----------------------------------------------
# -- pag 24 -------------------------------------
# -----------------------------------------------
# -- Filter -------------------------------------
# -----------------------------------------------


vegetables = [
    ['Beets', 'Cauliflower', 'Broccoli'],
    ['Beets', 'Carrot', 'Cauliflower'],
    ['Beets', 'Carrot'],
    ['Beets', 'Broccoli', 'Carrot']
    ]

def not_broccoli(food_list: list):
    return 'Broccoli' not in food_list
# retira as listas onde Broccoli surge
broccoli_less_list = list(filter(not_broccoli, vegetables))

print (broccoli_less_list)
# The filter() method constructs an iterator from elements 
# of an iterable for which a function returns true.


# -----------------------------------------------
# -- pag 25 -------------------------------------
# -----------------------------------------------
# -- Classes ------------------------------------
# -----------------------------------------------


class limp:
    a = [2, 4, 6]
    b = [7, 8]
    c = [10, 11, 12]
    def __init__(self, val):
        self.val = val
        # o valor dado por x, entra na função __init__ 
        print (val)
    def increase(self):
        self.val += 1
        # o valor dado por x (val += 1) 
    def show(self):
        print (self.val)

x = limp(6)
c = x.increase()
d = x.show()
print (type(x.a))
# mostra o tipo, neste caso lista, da variavel a dentro da class
print (x.a.__add__(x.b))
# junta a variavel a e b numa só lista
print (x.a.__eq__(x.b))
# is it a equal to b == False
print (x.a.__ne__(x.c))
# is it a not equal to c == True


# ------------------------------
# -------------------------------
# --- x.__lt__(y) -- x < y -------
# --- x.__le__(y) -- x <= y -------
# --- x.__eq__(y) -- x == y --------
# --- x.__ne__(y) -- x != y --------
# --- x.__gt__(y) -- x > y --------
# --- x.__ge__(y) -- x >= y ------
# -------------------------------
# ------------------------------
 

# -----------------------------------------------
