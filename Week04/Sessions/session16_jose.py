# -----------------------------------------------
# --------- Andreia Faria ----- Session 16 ------
# -----------------------------------------------
# -- 04.11.2020 ---------------------------------
# -----------------------------------------------
# -- pag 03 -------------------------------------
# -----------------------------------------------
# -- Numpy --------------------------------------
# -----------------------------------------------

'''
Numpy - fundamental package - for scientific computing
with python
Features:
    - typed multidimensional arrays (vector, matrices)
    - fast numerical computations (matrix math)
    - high-level math functions
'''

import numpy as np
x = np.array([[-1, 3],[4,2]])
y = np.array([[1,2], [3,4]])
z = np.dot(x,y)
print (z)


# -----------------------------------------------
# -- pag 04 -------------------------------------
# -----------------------------------------------


import numpy as np

print (np.__version__)
# mostra a versão do numpy
print (np.version)
# mostra a localização do numpy

dir(np)
dir(np.array)


# -----------------------------------------------
# -- pag 06 -------------------------------------
# -----------------------------------------------
# -- Arrays -------------------------------------
# -----------------------------------------------


students = np.array ([[[1, 3, 5], [1, 1, 1]],
                      [[4.5, 4, 5], [4.3, 4.4, 4.6]]])

print (students.ndim, students.dtype)
# 3 - numero de elementos
# float64 (bits)

'''
os arrays podem ser : np.int8, ao
                      np.int64,
                      np.float32,
                      np.float64.
'''

print (students.shape)
# output = (2, 2, 3)
# 2 = número de blocos
# 2 = número de linhas
# 3 = número de elementos


print (students.shape, '\n',
       students.nbytes, '\n',
       # 96 - shape*itemsize
       students.ndim, '\n',
       # 3 - número de elementos
       students.dtype, '\n',
       # float64 - tipo de ficheiro
       students.size, '\n',
       # 12 - numero de elementos 
       students.data, '\n',
       # <memory at 0x0000014B40A02E50> - localizacao das dados
       students.itemsize)
       # 8 
       

# -----------------------------------------------
# -- pag 07 -------------------------------------
# -----------------------------------------------
# -- Arithmetic operators -----------------------
# -----------------------------------------------


x1 = np.array([[-1, 3]])
x2 = np.array([[1, 2]])
x3 = x1+x2
x4 = x1*x2
x5 = x1-x2

print (x1, '\n', x2, '\n', x3, '\n', x4, '\n', x5)

# [[-1  3]] 
# [[1 2]] 
# [[0 5]] 
# [[-1  6]] 
# [[-2  1]]


# -----------------------------------------------
# -- pag 08 -------------------------------------
# -----------------------------------------------
# -- Data types ---------------------------------
# -----------------------------------------------


x3 = np.power(10, 4, dtype=np.int8)
print(x3)
x4 = np.power(10, 4, dtype=np.int16)
print(x4)

'''
numpy.int8      (-128 a 127)
numpy.int16     (-32768 a 32767)
....
é preciso ter atenção em não usar um tipo de dados
demasiado abrangente para o resultado que pretendemos.

'''


# -----------------------------------------------
# -- pag 09 -------------------------------------
# -----------------------------------------------
# -- ARRAY BROADCASTING -------------------------
# -----------------------------------------------


'''
The term broadcasting describes how numpy treats 
arrays with different shapes during arithmetic operations.
'''

a = np.array([1.0, 2.0, 3.0])
b = np.array([2.0, 2.0, 2.0])
print (a * b)
# [ 2.,  4.,  6.]

a = np.array([1.0, 2.0, 3.0])
b = 2.0
print (a * b)
# [ 2.,  4.,  6.]


# -----------------------------------------------
# -- pag 10 -------------------------------------
# -----------------------------------------------
# -- Reshape VS Resize --------------------------
# -----------------------------------------------


print(np.arange(10))
print(np.arange(10).reshape(2,5))
# cria um array de duas linhas e cinco elementos 
# por cada linha


print ((np.resize(np.arange(10), (3,7))))
# o resize adapta o array e se tiver necessidade 
# volta a aplicar o range até preencher o requesito  
# de ter 3 linhas com 7 elementos cada


# -----------------------------------------------
# -- pag 11 -------------------------------------
# -----------------------------------------------
# -- Newaxis / Copy -----------------------------
# -----------------------------------------------


d = np.arange(2,5)
print (d)  
# cria um array do range 2 ao 5
print (d.shape)
# (3,) - o array tem 3 elementos
print (d[:, np.newaxis])
# adiciona uma coluna(bloco) ao array
print (d[:, np.newaxis].shape)
# (3, 1) - 3 elementos, 1 bloco

x = np.array([1, 4, 3])
y = x
z = np.copy(x)
# cria uma cópia do array, o original fica inalterado
x[1] = 2
print(x)
print(y)
print(z)


# -----------------------------------------------
# -- pag 12 -------------------------------------
# -----------------------------------------------
# -- Storting -----------------------------------
# -----------------------------------------------


# ordenação de arrays tendo em conta
# um parametro específico.
import numpy as np
dtype = [('name', 'U10'), 
         ('grade', float), 
         ('age', int)]
values = [('Joseanne', 7, 31), 
          ('Hamed', 5, 32), 
          ('Stefan', 4, 24)]
sorted_data = np.array(values, dtype=dtype)
print (np.sort(sorted_data, order='age'))
print (np.sort(sorted_data, order='grade')[::-1])


'''
'?' ------ boolean ---------------------------------
'b' ------ (signed char) byte ----------------------
'B' ------ unsigned byte ---------------------------
'i' ------ (signed) integer ------------------------
'u' ------ unsigned integer ------------------------
'f' ------ floating-point --------------------------
'c' ------ complex-floating point ------------------
'm'	------ timedelta -------------------------------
'M' ------ datetime --------------------------------
'O' ------ (Python) objects ------------------------
'S', 'a' - zero-terminated bytes (not recommended) -
'U' ------ Unicode string --------------------------
'V' ------ raw data (void) -------------------------
'''

# o 'b' que surge no print indica o tipo de data 
# existente no array
# mudando o type do nome para um mais adequado 


# -----------------------------------------------
# -- pag 13 -------------------------------------
# -----------------------------------------------
# -- Zero (vectors/matrices) --------------------
# -----------------------------------------------


b = np.zeros((10,6), dtype=int)
print (b)
print (b.ndim, b.dtype, b.size)
b2 = np.zeros((10,6), dtype=float)
print (b2)

c = np.zeros(10)
c[8] = 1
# replace
print (c)

print (np.ones((3, 3)))


# -----------------------------------------------
# -- pag 14 -------------------------------------
# -----------------------------------------------
# -- Slice/Reshape ------------------------------
# -----------------------------------------------


b = np.arange(50)
b = b[::-1]
b = b.reshape(5,10)
print(b)
print(b[4,1])
print(b[3,:])
# faz print à linha
print(b[0,2:])
# faz print à linha 0 apartir do index do elemento 2
print(b[:4,3:])
# faz print das linhas com o index do 0 ao 3
# e todas elas começam apartir do index do elemento 3
print(b)


# -----------------------------------------------
# -- pag 16 -------------------------------------
# -----------------------------------------------

'''
--- concatenate -- join a sequence of arrays along an existing axis 
--------- stack -- join a sequence of arrays along a new axis
--------- block -- assemble an nd-array from nested lists of blocks
-------- hstack -- stack arrays in sequence horizontally
-------- dstack -- stack arrays in sequence depth wise (third axis)
-- column_stack -- stack 1D array as columns into a 2D array
-------- vsplit -- split an array into multiple sub-arrays vertically
'''

b = np.arange(50)
b = b.reshape(5,10)
c = np.ones((2,10))
print (c)
print (np.concatenate((b,c)))

print (np.vstack(c))
d = np.hstack(c)
print (np.vstack(d))

a = np.arange(2,6)
b = np.array([[3,5], [4,6]])
print (np.vstack(a))
print (np.hstack(b))


# -----------------------------------------------
# -- pag 17 -------------------------------------
# -----------------------------------------------
'''
---- empty_like -- return an empty array with shape and type of input
---------- ones -- return a new array setting values to one
--------- zeros -- return a new array setting values to zero
---------- full -- return a new array of given phape filled with value
'''

d = np.ones((1,3)) # [[1., 1., 1.]]
x = np.zeros_like((d)) # [[0., 0., 0.]]
x1 = np.ones_like((x)) # [[1., 1., 1.]]

b = np.eye(3)
print (b)
# é conhecida como matrix de identidade

print(np.empty((1,2)))
print(np.full((2,2), np.inf))


# -----------------------------------------------
# -- pag 18 -------------------------------------
# -----------------------------------------------
# -- Astype -------------------------------------
# -----------------------------------------------

# o astype é para configurar o tipo de variaveis

b = np.arange(50)
c = b.astype(np.int16)
print(b.ndim, b.dtype, b.size)
print(c.ndim, c.dtype, c.size)


# -----------------------------------------------
# -- pag 19 -------------------------------------
# -----------------------------------------------
# -- Random Numbers -----------------------------
# -----------------------------------------------

# o random não é assim tão random quanto pode dar a entender

b = np.random.random((10, 10))
bmin, bmax = b.min(), b.max()
print(bmin, bmax)

d = np.random.random((3,3,3))
print(d)


# -----------------------------------------------
# -- pag 20 -------------------------------------
# -----------------------------------------------
# -- Usefull funcionality -----------------------
# -----------------------------------------------

# bom para transaçoes bancarias, np.datetime64
# o np.datetime é mais optimizado que o datetime do python
yesterday = np.datetime64('today') - np.timedelta64(1)
today = np.datetime64('today')
tomorrow = np.datetime64('today') + np.timedelta64(1)
print (yesterday, today, tomorrow)
b = np.arange('2019', '2021', dtype='datatime64[D]')
print (b.size, b.ndim)

print(np.ones((2,6))+np.ones((2,6)))

x1 = np.array([[-1,3],[4,2]])
x2 = np.array([[1,2],[3,4]])
x4 = x1+x2
x5 = x1*x2
x3 = np.dot(x1,x2)
print(x3)
print(x4)
print(x5)


# -----------------------------------------------
# -- pag 21 -------------------------------------
# -----------------------------------------------


# os grafico são importantes para mostrar desempenho do prog

import numpy as np
import matplotlib.pyplot as plt
# este exmp é bom para avaliar estatisticas
values = np.linspace(-(2*np.pi), 2*np.pi, 20)
# 2*pi = 6.28, logo o range dele vais do 6.28 positivo ao negativo
# o 20 neste caso é equivalente aos pontos que queremos ter
# quase como pontos num vector para criar uma esfera mais bonita
cos_values = np.cos(values)
sin_values = np.sin(values)

plt.plot(cos_values, color = 'blue', marker = '*')
plt.plot(sin_values, color = 'red')

plt.show()


# -----------------------------------------------
# -- pag 22 -------------------------------------
# -----------------------------------------------
# -- Image processing ---------------------------
# -----------------------------------------------


import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

path = 'C:\\Users\\Huga\\Desktop\\PythonCourse\\Week04\\session16_photo.jpg'
jpg = Image.open(path)
plt.imshow(jpg)

# jpg2 = np.array(jpg.convert('P'))
# plt.imshow(jpg2)

# jpg3 = np.array(jpg.convert('L'))
# plt.imshow(jpg3)

# jpg4 = np.array(jpg.quantize(100))
# plt.imshow(jpg4)

print(jpg.format, jpg.size, jpg.mode)


def split(self):
    """
    Split this image into individual bands. This method returns a
    tuple of individual image bands from an image. For example,
    splitting an "RGB" image creates three new images each
    containing a copy of one of the original bands (red, green,
    blue).

    If you need only one band, :py:meth:`~PIL.Image.Image.getchannel`
    method can be more convenient and faster.

    :returns: A tuple containing bands.
    """

    self.load()
    if self.im.bands == 1:
        ims = [self.copy()]
    else:
        ims = map(self._new, self.im.split())
    return tuple(ims)



# -----------------------------------------------