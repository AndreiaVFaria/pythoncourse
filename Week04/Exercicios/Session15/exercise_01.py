# -----------------------------------------------
# --------- Andreia Faria -----------------------
# -----------------------------------------------
# -- Session 15 -- Exercise 01 ------------------
# -----------------------------------------------

'''
- In the module that we created,  
provide operations with circles, namely
, calculation of the perimeter and area given the radius.
Add the sphere volume function code in your module
- Provide operations with squares, namely, 
calculation of the perimeter and area given the length of the side.
Provide the possibility to ask the user for a value 
and writes on the screen the perimeter and the area 
of the circle and square based in the radius and side measurement.
'''



# -----------------------------------------------
