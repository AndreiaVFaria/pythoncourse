# -----------------------------------------------
# --------- Andreia Faria -----------------------
# -----------------------------------------------
# -- Session 15 -- Exercise 02 ------------------
# -----------------------------------------------

'''
Write a function that takes as a parameter a list of integers. 
The function must return a tuple with two integer values f1 and f2, 
where f1 is the list element with lowest frequency 
(lowest number of occurrences in the list) and f2 is the element
 with the highest frequency. 
 
Tip: use a dictionary to compute the frequencies of the list elements.

The following function must be implemented:
def frequencies (v)

Input =  [1, 4, 5, 1, 6, 3, 2, 1, 2, 9, 1, 4, 6, 3, 9]
Output = ([f1], [f2])     f1 = [5,7]     f2=[1]

def frequencies (v)
'''


def frequencies(v): 

    d = {}
    for i in v:
        d[i] = d.get(i, 0)+1
    temp = min(d.values()) 
    f1 = [key for key in d if d[key] == temp] 
    f2 = max(d, key=d.get)
    return (f1, f2)



numbers = [1, 4, 5, 1, 6, 3, 2, 1, 2, 9, 1, 4, 6, 3, 7, 9]
print (frequencies(numbers))



# -----------------------------------------------
