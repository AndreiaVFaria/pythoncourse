# -----------------------------------------------
# --------- Andreia Faria ----- Session 16 ------
# -----------------------------------------------
# -- Ex Gráficos --------------------------------
# -----------------------------------------------
'''
 Também, está no anexo um arquivo csv com os dados do PISA de 
matemática de Portugal (avaliação global da educação)
Gostaria de ver um gráfico de médias obtidas por ano de portugal (PRT), 
 um outro de 5 outros os países(livre escolha) comparados com Portugal 
 e um terceiro gráfico de barras com os dados em relação ao desempenho 
 de meninos e as meninas usando o matplotlib   
 https://data.oecd.org/pisa/mathematics-performance-pisa.htm#indicator-chart
'''


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

path = 'C:\\Users\\Huga\\Documents\\Python_ISCTE\\Aulas\\Week_04\\DP_LIVE_04112020161547500.csv'
data = pd.read_csv(path)

prt_fil = data[(data.LOCATION == 'PRT')]



Boys = prt_fil[(prt_fil.SUBJECT == 'BOY')]
Boy = Boys['Value'].tolist()

Girls = prt_fil[(prt_fil.SUBJECT == 'GIRL')]
Girl = Girls['Value'].tolist()
Year = Girls['TIME'].tolist()


# x = data[(data.TIME)]
# y = data[(data.Value)]

plt.plot(Year, Boy, color = 'green')
plt.plot(Year, Girl, color = 'orange')

plt.legend(['Boys', 'Girls'])
plt.title('Grades by Years (PRT)')
# plt.ylabel('Grades')
# plt.xlabel('Years')
# plt.show()

# -----------------------------------------------
