# -----------------------------------------------
# --------- Andreia Faria ----- Session 16 ------
# -----------------------------------------------
# -- 04.11.2020 -- Git Ex -----------------------
# -----------------------------------------------
'''
git rm -r(remove os ficheiros do repositorio e do directorio de trabalho,
          este comando funciona para pastas e ficheiros)
git revert <commit desejado> 
git checkout -b Geo (nome do novo branch)
git checkout Geo
git mv Area Geo
git mv Drawing Geo
git commit -m "folders moved"

git revert - retorna a um commit desejado mas mantem modificações posteriores
git reset - move a Head para o commit desejado, no entanto corre o risco de deixar metadata perdida
'''