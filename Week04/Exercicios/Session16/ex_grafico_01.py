# -----------------------------------------------
# --------- Andreia Faria ----- Session 16 ------
# -----------------------------------------------
# -- Ex Gráficos --------------------------------
# -----------------------------------------------
'''
 Também, está no anexo um arquivo csv com os dados do PISA de 
matemática de Portugal (avaliação global da educação)
Gostaria de ver um gráfico de médias obtidas por ano de portugal (PRT), 
 um outro de 5 outros os países(livre escolha) comparados com Portugal 
 e um terceiro gráfico de barras com os dados em relação ao desempenho 
 de meninos e as meninas usando o matplotlib   
 https://data.oecd.org/pisa/mathematics-performance-pisa.htm#indicator-chart
'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


path = 'C:\\Users\\Huga\\Documents\\Python_ISCTE\\Aulas\\Week_04\\DP_LIVE_04112020161547500.csv'
dataF = pd.read_csv(path)

# Quero ficar com PRT, ESP, ITA, FRA, DNK, FIN

# dataF.info()
# LOCATION - object
# SUBJECT - object
# TIME - int64
# Value - float64

df = dataF[dataF['LOCATION'].isin(['PRT', 'ESP', 'ITA', 'FRA', 'DNK', 'FIN'])]
df_g = df[df['SUBJECT'].isin(['GIRL'])]
dfinal = df_g[['LOCATION', 'TIME', 'Value']]


# tornar isto numa função:
prt = dfinal[(dfinal.LOCATION == 'PRT')]
esp = dfinal[(dfinal.LOCATION == 'ESP')]
ita = dfinal[(dfinal.LOCATION == 'ITA')]
fra = dfinal[(dfinal.LOCATION == 'FRA')]
dnk = dfinal[(dfinal.LOCATION == 'DNK')]
fin = dfinal[(dfinal.LOCATION == 'FIN')]

x = prt['TIME'].tolist()
PRT = prt['Value'].tolist()
ESP = esp['Value'].tolist()
ITA = ita['Value'].tolist()
FRA = fra['Value'].tolist()
DNK = dnk['Value'].tolist()
FIN = fin['Value'].tolist()

with plt.style.context('Solarize_Light2'):
    plt.plot(x, PRT, marker='o', color = '#095859')
    plt.plot(x, ESP, marker='o', color = '#e9d75a')
    plt.plot(x, ITA, marker='o', color = '#0FD56C')
    plt.plot(x, FRA, marker='o', color = '#AF1A87')
    plt.plot(x, DNK, marker='o', color = '#1B07D1')
    plt.plot(x, FIN, marker='o', color = '#B90A0A')
    plt.ylabel('Grades', fontname='Century')
    plt.xlabel('Years', fontname='Century')
    plt.legend(['PRT', 'ESP', 'ITA', 'FRA', 'DNK', 'FIN'], loc=1, fontsize = 'x-small')
    plt.title('Comparison of Grades', fontname='Century', fontsize=18)
    

plt.show()



# -----------------------------------------------