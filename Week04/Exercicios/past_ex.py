# -----------------------------------------------
# -----------------------------------------------
# --------- Andreia Faria -----------------------
# -----------------------------------------------
# -- week01 -- Exercicios_lista_2 -- ex 1 -------
# -----------------------------------------------


n1 = float(input('Enter first number:'))
n2 = float(input('Enter second number:'))
operator = input('These are your choises +, -, * or /.\nEnter one operator:')
f = 'The result is:'

if operator == '+':
    print (f, (n1+n2))
elif operator == '-':
    print (f, (n1-n2))
elif operator == '*':
    print (f, (n1*n2))
elif operator == '/':
    print (f, (n1/n2))
else:
    print ('Not a valid operator.\nTry +, -, * or /.')


# -----------------------------------------------
# -- Be careful using conditionals --------------
# -----------------------------------------------


day = "Saturday"
temperature = 24
raining = True

if day == "Saturday" and temperature > 20 and raining == False:
    # se não tiver a referencia (raining == False) o prog fica com bug
    print ("Go out ")
else:
    print ("Better finishing python programming exercises")


# -----------------------------------------------
