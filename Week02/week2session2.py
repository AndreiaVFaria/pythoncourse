# -----------------------------------------------
# --------- Andreia Faria ----- Session 2 -------
# -----------------------------------------------
# -- pag 20 -------------------------------------
# -----------------------------------------------


dir() 

import math
dir (math) # exectutar na consola
math.pi

s = 'a'
dir(s)

def my_func():
    '''
    we can describe what my func does
    '''
    print ('Python')
    
# my_func()
# after we run in the console type help(my_func) 
# my_func.__doc__
# my_func??


# -----------------------------------------------
# -- pag 24 -------------------------------------
# -- ex 1 ---------------------------------------
# -----------------------------------------------


import numpy as np
from matplotlib import pyplot as plt
data = np.array ([-20, -3, -2, -1, 0, 1, 2, 3, 4])
plt.boxplot(data)

'''
from numpy import * 
not a good idea because imports the entire module
'''

# -----------------------------------------------
# -- pag 27 -------------------------------------
# -----------------------------------------------


import random
import math
for i in range(5):
    print (random.randint(1, 25))
print(math.pi)


# -----------------------------------------------
# -- pag 30 -------------------------------------
# -----------------------------------------------


a = 3
def square(a):
    return a**2
print (square(a))


# -----------------------------------------------
# -- pag 32 -------------------------------------
# -- ex 2 ---------------------------------------
# -----------------------------------------------


from math import fmod, gcd, fabs

help(fmod) 
help (gcd) 
help (fabs)

print (fmod(9, 4)) # Return fmod(x, y), according to platform C.
print (gcd(30, 4)) # greatest common divisor of x and y
print (fabs(-6)) # Return the absolute value of the float


from random import randint, choice, shuffle

print (randint(1, 5)) # Return random integer in range [a, b]
print (choice([1, 5])) # Choose a random element from a non-empty sequence.
a = [1, 2, 3, 4]
print (shuffle(a)) # Shuffle list a in place, and return None.


# -----------------------------------------------
# -- pag 34 -------------------------------------
# -----------------------------------------------


import random
print (random.randint(1, 5))
print (random.choice([1, 5]))

a = [1, 2, 3, 4]

random.shuffle(a) 
print (a)

import sys
print (sys.version)
print (sys.platform)


# -----------------------------------------------
# -- pag 36 -------------------------------------
# -----------------------------------------------


import datetime

now = datetime.datetime.now()

print (now)
print (now.year)
print (now.month)
print (now.day)

print (datetime.datetime.today())


# -----------------------------------------------
# -- pag 38 -------------------------------------
# -- ex 3 ---------------------------------------
# -----------------------------------------------


import datetime
m = now.minute

print (m)
if m % 2 == 0:
    print ('Even minute.')
else:
    print ('Odd minute.')


m = datetime.datetime.today()
n = m.minute
print (m)
print (n)
if n % 2 == 0:
    print ('Even minute.')
else:
    print ('Odd minute.')


# consola datetime.datetime.today??
# datetime.datetime.today(). perguntar ao stefan pq que não dá a info do tab


from datetime import datetime as dt

m = dt.today()
n = m.minute
print (m)
print (n)
if n % 2 == 0:
    print ('Even minute.')
else:
    print ('Odd minute.')


# One solution ----------------------------------
# -----------------------------------------------

from datetime import datetime as dt
m = dt.today().minute
if m % 2 == 0:
    print ('Not an odd minute.')
else:
    print ('Odd minute.')

# Another solution for loop ---------------------
# -----------------------------------------------

from datetime import datetime as dt
m = dt.today().minute
print (m)
for i in range(1, 61, 2):
    if m == i:
        print ('Not an odd minute.')
        break
    else:
        print ('Odd minute.')  
        break
  
# Another solution with list --------------------
# -----------------------------------------------

from datetime import datetime as dt
m = dt.today().minute
odds_list = [ i for i in range(1, 60, 2)] # run to se if its ok
print (m)
if m in odds_list:
    print ('Odd minute.')
else:
    print ('Not an odd minute.')

 

