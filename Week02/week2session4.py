# -----------------------------------------------
# --------- Andreia Faria ----- Session 4 -------
# -----------------------------------------------
# -- pag 00 -------------------------------------
# -----------------------------------------------


a = (1, 2, 3)

for i in a:
    i = a[0]
    i = a[1]
    i = a[2]

if i in a:
    i == a[0]
    i == a[1]
    i == a[2]

# if ( i == a[0] or i == a[1] or i == a[2])
   

# -----------------------------------------------
# -- pag 18 -------------------------------------
# -----------------------------------------------
# -- tuples -- access members -------------------
# -----------------------------------------------


t = ('History', 'Art', 'Cinema')
for i in t:
    print (f'I like to read about {i}')


# -----------------------------------------------
# -- pag 18 -------------------------------------
# -----------------------------------------------
# -- tuples --*, +, reverse ---------------------
# -----------------------------------------------


t = (1, 9, 2)
print (t*2)
print (t + t + t)
print (tuple(reversed(t)))


# -----------------------------------------------
# -- pag 25 -------------------------------------
# -----------------------------------------------
# -- Exercise 1 ---------------------------------
# -----------------------------------------------


# input(tuple) t = (4, 6)
# convert tuple to list
# add/append 9 into the list
# once the list is created, list needs to be transformed to tuple
# output t be (4, 6, 9)

t = (4, 6)
y = list(t)
# print (y)
y.append(9) # y.extend([9])
# print (y)
t = tuple(y)
print (t)


# -----------------------------------------------
# -- pag 29 -------------------------------------
# -----------------------------------------------
# -- Exercise 2 ---------------------------------
# ----------------------------------------------- 


# input t = '$ $ Python$#Course $'
# output t = 'Python Course'
# strip ()


# t = '$ $ Python$#Course $'

# t = t.replace('$','') 
# t = t.replace('#',' ')
# print (t)

# dir(t) -- mostra todos os metodos possiveis

# t = t.strip('$ #')
# t = t.replace('$#', ' ')
# print (t)


t = '$ $ Python$#Course $'
#t = t.strip('$ #')
a = list(t)
b = a.copy()

for i in a:
    if i == '#' or i == '$' or i == ' ': #if i in ['#', '$', '\n', '\t']:
        b.remove(i)

#b.insert(index, ' ',)
#b.index('C') usar na consola para saber o valor do index

C_index = b.index('C')
b.insert(C_index, ' ')

t = ''.join(b) # converter uma list numa string
print (t)


# -----------------------------------------------
# -- pag 33 -------------------------------------
# -----------------------------------------------


# só é possivel remover um membro de um tuple se o converter para outro tipo
t = (4, 7, 2, 9, 8)
l = list(t)
l.remove(9)
t = tuple(l)
print (t)


# -----------------------------------------------
# -- pag 34 -------------------------------------
# -----------------------------------------------


t = (4, 8)
a, b = t
print (a)
print (b)

car = ('blue', 'auto', 7)
color, _, a = car
print (color)
print (_)
print (a)


x = (4, [3, 4], 8)
a, b, c = x
print (a)
print (b)
print (c)


# -----------------------------------------------
# -- pag 35 -------------------------------------
# -----------------------------------------------
# -- Zip ----------------------------------------
# -----------------------------------------------


a = (1, 2)
b = (3, 4)
c = zip(a,b)
x = list(c)

print (x)
print (x[0])
print (type(x[1]))


z = ((1, 3), (2, 4)) # experimentar este metodo com mais elementos nas listas
u = zip(*z)
print (list(u))


z = {
     'brand' : 'cherry' ,
     'model' : ' arizo 5' ,
     'color' : 'white'
     }
u = zip(*z)

print (list(u))


a = [1, 2, 'A']
b = ('Python', 161.8, 0, 5)
c = {10, 12, 14, 16, 18, 20} # set - undorderer e não tem index

# print (list(zip(a, b, c)))

# escrever um prog para ter este output sem usar o zip
# output -- [(1, 'Python', 10), (2, 161.8, 12), ('A', 0, 14)]
# 1 - encontrar o min da len de a, b, c
# na consola - len(a), len(b), len(c) -- min(len(a), len(b), len(c))

min_abc = min(len(a), len(b), len(c))

# output:
# [(a[0], b[0], c[0]), (a[1], b[1], c[1]), (a[2], b[2], c[2])]

c = list(c)


i = 0
d1 = [(a[0], b[0], c[0]), (a[1], b[1], c[1]), (a[2], b[2], c[2])]
d2 = [(a[i], b[i], c[i]), 
      (a[i+1], b[i+1], c[i+1]), 
      (a[i+2], b[i+2], c[i+2])]

e = []
for i in range(min_abc):
    my_tuple = (a[i], b[i], c[i])
    e.append(my_tuple)
    
output = e
print (output)


# -----------------------------------------------
# -- pag 38 -------------------------------------
# -----------------------------------------------
# -- Exercise 3 ---------------------------------
# ----------------------------------------------- 


a = [1, 2, 'A']
b = ('Python', 161.8, 0, 5)
c = {10, 12, 14, 16, 18, 20}

min_abc = min(len(a), len(b), len(c))
c = list(c)

e = []

for i in range(min_abc):
    my_tuple = (a[i], b[i], c[i])
    e.append(my_tuple)

print (e)


# -----------------------------------------------
# -- pag 40 -------------------------------------
# -----------------------------------------------


# how many tuples are in the list
num = [8, 2, (9, 3), 4, (1, 6, 7), 34]

c = 0
for i in num:
    if type (i) == tuple:
        c += 1


# -----------------------------------------------
# -- pag 42 -------------------------------------
# -----------------------------------------------
# -- Exercise 4 ---------------------------------
# ----------------------------------------------- 


num = [8, 2, (9, 3), 4, (1, 6, 7), 34]

c = 0

for i in num:
    if type(i) == tuple:
        c += 1
print ('There are', c, 'tuples in num.')


# -----------------------------------------------
# -- pag 46 -------------------------------------
# -----------------------------------------------
# -- Homework 1 ---------------------------------
# ----------------------------------------------- 


a = [(1, 2, 3), (4, 5, 6)]
new_list = []

for i in a:
    l = list(i)
    new_list.append(l)
    for i in new_list:
        i.remove(i[2])
        i.append(9)
a = tuple(new_list)
print (a)


# -----------------------------------------------
# -- pag 48 -------------------------------------
# -----------------------------------------------
# -- Homework 2 ---------------------------------
# ----------------------------------------------- 


my_obj = [(2, 4)]

for i, j in my_obj:
    print ('i is: ', i)
    print ('j is: ', j)


# -----------------------------------------------
# -- pag 50 -------------------------------------
# -----------------------------------------------
# -- Homework 3 ---------------------------------
# -----------------------------------------------


a = [(1, 3), (2, 4), ('A', 8)]
# output -- [(1, 2, 'A'), (3, 4, 8)]

A, B, C = a
new_list = []

for i in range(len(a)-1):
    D = (A[i], B[i], C[i])
    new_list.append(D)
print(new_list)



