
'''
---------- Andreia Faria -----------------------
------------------------------------------------
--- pag 00 -------------------------------------
------------------------------------------------

import numpy as np
# import pandas as pd
# import matplotlib.pyplot as plt
from matplotlib import pyplot as plt # another way to do it

data = np.array ([-20. -3, -2, -1, 0, 1, 2, 3, 4])
plt.boxplot (data)

'''

# -----------------------------------------------
# --------- Andreia Faria ----- Session 1 -------
# -----------------------------------------------
# -- pag 17 -------------------------------------
# -----------------------------------------------
# -- Variable Examples --------------------------
# -----------------------------------------------


name ='Josh'
age = 31.6

print('%s is %d years old' % (name, age))
# %s - placeholder for a string
# %d - placeholder for a number

print('{} is {} years old'.format(name, age))
print(f'{name} is {age} years old')

print(name + ' is ' + str(age) + ' years old') # all should be string
print(name, 'is', int(float(age)), 'years old') # solution to convert in hole numbers


# -----------------------------------------------
# -- pag 21 -------------------------------------
# -----------------------------------------------
# -- Multi assignment ---------------------------
# -----------------------------------------------

a = 5
b = 1
print (f'Five plus one is {a + b}.')

a = b = c = 5
print(a, b, c) # a, b and c are independent

x = 1
y = 2
y, x = x, y # make them switch value
print (x)
print (y)


# -----------------------------------------------
# -- pag 25 -------------------------------------
# -----------------------------------------------
# -- Data Types Examples ------------------------
# -----------------------------------------------


l = ['apples', 'grapes', 'oranges']
t = ('apple', 'banana', 'cherry')
d = {'id': '123', 'name': 'jonathan'}
s = {'apple', 'banana', 'cherry'}

print (type(l))
print (type(t))
print (type(d))
print (type(s))


# -----------------------------------------------
# -- pag 26 -------------------------------------
# -----------------------------------------------


a = int(input('Enter a:'))
b = int(input('Enter b:'))
c = a + b
print (c)

n = 15.7
print ('%i' % n) # integer decimal
print ('%f' % n) # floating point decimal format
print ('%e' % n) # floating point exponential format


# -----------------------------------------------
# -- pag 29 -------------------------------------
# -----------------------------------------------
# -- Arithmetic Operators Examples --------------
# -----------------------------------------------


print (3 / 2) # float division
print (3 // 2) # integer division
print (17 % 5) # remainder
print (2 ** 3) # exponentiation
 

# -----------------------------------------------
# -- pag 33 -------------------------------------
# -----------------------------------------------
# -- Bitwise Operators Examples -----------------
# -----------------------------------------------


x = [1, 2, 3, 4, 5]
print (3 in x)
print (10 not in x)

a = 13 
print (bin(a))
b = 14
print (bin(b))
c = a | b 
print (bin(c))
c = a & b
print (bin(c))
c = a ^ b
print (bin(c))


a = 13
print (a << 1) # shift to left

a = 20 
print (a >> 1) # shift to right

a = 18
print (a >> 2) # shift 2 bits to right


# -----------------------------------------------
# -- pag 38 -------------------------------------
# -----------------------------------------------
# -- Control Statements Examples ----------------
# -----------------------------------------------


x = 1
y = 2
if x == 1 or y == 1:
    print ('ok')
else:
    print ('no')
    
names = ['sara', 'taha', 'farshid']
if 'ali' in names:
    print ('found')
else:
    print ('not found')


# -----------------------------------------------
# -- pag 39 -------------------------------------
# -----------------------------------------------
# -- Conditional Expression ---------------------
# -----------------------------------------------


a = 2
b = 5
#if a < b:
#    m = a
#else: 
#    m = b
m = a if a < b else b
print (m)

my_list = ['a', 'e', 'i', 'o', 'u']
#if 'o' in my_list:
#    s = 'yes'
#else:
#    s = 'no'
s = 'yes' if ('o' in my_list) else 'no' 
print (s)

x = 14
y = 6
z = 1 + (x if x > y else y+2)
print (z)

grade = 7
s = 'fail' if grade < 10 else 'pass'
print (s)


# -----------------------------------------------
# -- pag 45 -------------------------------------
# -----------------------------------------------
# -- for / range examples -----------------------
# -----------------------------------------------


for j in range (5,10,2):
    print (j , end = ' ')

s = 'Python'
for ch in s:
    print (ch)

for _ in range(3):
    print ('hello') # prints hello * 3
    
for i in range (4):
    print (i , end = ' ')
    
for i in range (3, 8):
    print (i , end = ' ')

word = 'python'
c = 0
for i in word:
    c += 1
print (c)

word = 'alirezaee'
c = 0
for i in word:
    if i == 'e':
        c+=1
print (c)

name = 'farshid'
v = 'aeiou'
c = 0
for ch in name:
    if ch in v:
        print (ch)
        c+=1
print (c)


# -----------------------------------------------
# -- pag 50 -------------------------------------
# -----------------------------------------------
# -- Nested for / range examples ----------------
# -----------------------------------------------


for i in range (1, 4):
    for j in range (2, 4):
        print (j, end=' ')
    print (i)


# -----------------------------------------------
# -- pag 52 -------------------------------------
# -----------------------------------------------
# -- while (break / continue) examples ----------
# -----------------------------------------------


i = 1
while i <= 3:
    print (i, end=' ')
    i += 1
print (i)
    
n = 7
while n >= 3:
    print (n, end=' ')
    n -= 1
print (n)

s = 'abcdef'
i = 0
while True:
    if s[i] == 'd':
        break
    print (s[i], end=' ')
    i+=1
print (i)

n = 8
while n > 2:
    n-=1
    if n == 4:
        break
    print (n, end=' ')
print (n)


# -----------------------------------------------
# -- pag 57 -------------------------------------
# -----------------------------------------------


import random
n = random.randrange(0,10)
f = 'no'
#print (n)

while f == 'no':
    a = int(input('Please guess number between 0 and 9:'))
    if a < n:
        print ('increase')
    elif a > n:
        print ('decrease')
    else:
        print ('Correct, You won')
        f = 'yes'
print ('Thank you.')



