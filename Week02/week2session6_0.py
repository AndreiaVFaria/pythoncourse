# -----------------------------------------------
# --------- Andreia Faria ----- Session 6 -------
# -----------------------------------------------
# -- pag 51 -------------------------------------
# -----------------------------------------------
# -- Exercise 6 ---------------------------------
# -----------------------------------------------


s = 'Python Course'
x = ['o', 'r']
d = {}

for i in s:
    if i in x:    
        d[i] = d.get(i, 0) +1

print (d)

# -- another solution -- 


s = 'Python Course'
x = ['o', 'r']
d = {}

for i in s:
    if i in x: 
        d.setdefault(i,0)
        d[i] += 1
print (d)


# -----------------------------------------------
# -- pag 55 -------------------------------------
# -----------------------------------------------
# -- Exercise 7 ---------------------------------
# -----------------------------------------------


d = {'x': 3, 'y': 2, 'z': 1, 'y': 4, 'z': 2}
r = {}

print (d)
for k, v in d.items():
    if k not in r.keys():
        r[k] = v
print (v)

# dictonary doesn't accept duplicate keys


# -----------------------------------------------
# -- pag 58 -------------------------------------
# -----------------------------------------------
# -- Exercise 8 ---------------------------------
# -----------------------------------------------


# list of dictonary
students = [
            {'id':123, 'name': 'Sophia', 's': True},
            {'id':378, 'name': 'William', 's': False},
            {'id':934, 'name': 'Sara', 's': True}
            ]

# we can use calculation in boolean True and False
# print (True*3)

# students[0]['s'] -- consola
sum_of_True = 0
for i in students:
    sum_of_True += int(i['s'])

print (sum_of_True)

