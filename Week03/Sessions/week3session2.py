# -----------------------------------------------
# --------- Andreia Faria ----- Session 2 -------
# -----------------------------------------------
# -- 27.10.2020 ---------------------------------
# -----------------------------------------------

# --------------------- ex 1


f = float(input('Please enter temperature'))
c = (f - 32) / 1.8
print (c)


# --------------------- ex 2


yearB = int(input('Please enter your year of birth:'))
print ('Your age a year from now is:', 2020 - yearB)


# --------------------- ex 3


h = '01:30:22'
h_comp = h.split(':')
seg_total = int(h_comp[0])*3600 + int(h_comp[1])*60 + int(h_comp [2])
print (seg_total)


# --------------------- ex 4


x = 1
if x == 1:
    x = x + 1   # x = 2
    if x == 1:
        x = x + 1
    else:
        x = x - 1
else:
    x = x - 1   # x = 1
# o segundo if nunca é executado


# --------------------- ex 5


# pensar o que é que a numeração romana tem em particular 
num = int(input('escreva um numero inteiro:'))
roman_num = ''
d_roman = {
        1:'I',
        4:'IV', 5:'V', 9:'IX', 10:'X',
        40:'XL', 50:'L', 90:'XC', 100:'C',  
        400:'CD', 500:'D', 900:'CM', 1000:'M' 
        }          

num_ord = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]

for x in num_ord:
    quociente = num // x
    if quociente != 0:
        for _ in range(quociente):
            roman_num += d_roman[x]
    num = num % x

print (roman_num)


# --------------------- ex 6


'''
escrever um prog que leia um ano (>0) e escreva
o século a que pertence

'''

# ano = int(input('Escreva um ano:'))
ano = 2000
temp = ano // 100
resto = ano % 100
print (temp if resto == 0 else temp +1)


# --------------------- ex 7


'''
escrever um prog que recebe ano, mes, dia
em separado, e um numero de dias x, e 
devolva nova data x dias mais tarde
ano bisexto se é divisivel por 4 

'''
ano = int(input('Ano:'))
mes = int(input('Mês:'))
dia = int(input('Dia:'))
dias_add = int(input('Que dia será daqui a este numero de dias?'))

meses_dias = { 
             1:31, 2:28, 3:31, 4:30, 5:31, 6:30,
             7:31, 8:31, 9:30, 10:31, 11:30, 12:31
             }

dias_do_mes = meses_dias[mes]

if mes == 12:
    dias_do_mes = meses_dias[mes]
    if dia + dias_add > dias_do_mes:
        mes = 1
        dia = dias_add - (dias_do_mes - dia)
        ano += 1
    else:
        dia = dia + dias_add
else:
    if ano != 4 and not (ano != 100 and ano !=400):
        dias_do_mes = 0
        if mes == 2:
            dias_do_mes = meses_dias[mes] + 1
        else:
            dias_do_mes = meses_dias[mes]
        if dia + dias_add > dias_do_mes:
            mes = 1
            dia = dias_add - (dias_do_mes - dia)
            ano += 1
        else:
            dia = dia + dias_add 
    else:
        dias_do_mes = meses_dias[mes]
        if dia + dias_add > dias_do_mes:
            while dia > dias_do_mes:
            mes = 1
            dia = dias_add - (dias_do_mes - dia)
            ano += 1
        else:
            dia = dia + dias_add

print(f'{ano}-{mes}-{dia}')

# -----------------------------------------------
