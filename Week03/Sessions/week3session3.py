# -----------------------------------------------
# --------- Andreia Faria ----- Session 3 -------
# -----------------------------------------------
# -- 28.10.2020 ---------------------------------
# -----------------------------------------------
# -- functions and modules in python ------------
# -----------------------------------------------


def multiplica_num (numero, valor = 2):
    return numero * valor

print (multiplica_num(2))
print (multiplica_num(2, 4))


# -----------------------------------------------


a = [['a', 2], ['asf', 0], ['askdhfshjbh', 25], ['asdgadf', 0]]

def compare_items (item):
    return len(item[0])
a.sort(key=compare_items)
print (a)


# -----------------------------------------------
# -- 3 funçoes de alto nível que fazem ----------
# -- parte da standert library ------------------
# -----------------------------------------------
# -- map(), filter(), sorted() ------------------
# -----------------------------------------------


# -----------------------------------------------
# -- map() --------------------------------------


def multiply (a, b):
    return a * b

a = list(map(multiply, (1,2,3), (2,3,4)))


# -----------------------------------------------
# -- filter() -----------------------------------


a = [1, 22, 33, 2]
def lt_dezasseis (x):
    return x < 16

filtered_a = list(filter(lt_dezasseis, a))

# esta função retorna um bollean


# -----------------------------------------------
# -- sorted() -----------------------------------


a = ['abc', 'jhdfj', 'js', 'kajshfkjashfksj']
sorted_a = list(sorted(a,key=len))
print (sorted_a)

sorted_a = list(sorted(a,key=len, reverse=True))
print (sorted_a)
# é mesmo necessario escrever o Key e o Reverse


# -----------------------------------------------
# -- reduce() -----------------------------------


from functools import reduce
fruits = ['apple', 'cherry', 'peach']
sumed_fruits = reduce(add,fruits,'I love')
print (sumed_fruits)


# -----------------------------------------------


a = [[1,2], [3,4]] # se tiver mais listas é só adicionar mais for
flattened_a = [sub_item for item in a for sub_item in item]

# for item in a:
#     for sub_item in item:
#         print (sub_item)

from functools import reduce
flattened_a = reduce(lambda x,y: x + y, a)


# -----------------------------------------------
# -- folha5.pdf ---------------------------------
# -----------------------------------------------
# -- ex 1 ---------------------------------------

def divisao_int (x, y):
    '''
    

    Parameters
    ----------
    x : TYPE - int
        DESCRIPTION. - dividendo
    y : TYPE - int
        DESCRIPTION. - divisor

    Returns
    -------
    None. - int: resultado da divisão inteira de x por y

    '''


# -----------------------------------------------
# -- ex 2 ---------------------------------------
# -- b) --


def potenciaP (a):
    return a ** a

a = 2
print (potenciaP(a))


# -- c) --


def potencia (a, b = None):
    return a**a if b == None else a**b

a = 2
b = None

print (potencia(a,b))

# tem qlq coisa mal

# -- ex ---------------------------------------


ano_atual = 
mes_atual = 
dia_atual = 

ano = int(input('Introduza o ano de nascimento:'))
mes = int(input('Introduza o mes de nascimento:'))
dia = int(input('Introduza o dia de nascimento:'))


def datas (ano, mes, dia):
    if mes > mes_atual or mes == mes_atual and dia > dia_atual
    return 
    










# -----------------------------------------------






# -----------------------------------------------
