# -----------------------------------------------
# --------- Andreia Faria ----- Session 5 -------
# -----------------------------------------------
# -- 30.10.2020 ---------------------------------
# -----------------------------------------------
# -- Classes e objectos -------------------------
# -----------------------------------------------


class House: 
    # sintax para defenir uma classe - a classe cria um objecto
    area = 120 # variaveis da classe
    window_size = (2,3)
    
    def abre_porta(self): # funçoes da classe 
        pass # indica que há codigo em falta
    # variaveis e funçoes de uma classe são chamados atributos
    # tudo o que está de uma função são atributos
 
    def __init__(self, cor_das_paredes, cor_do_telhado): 
        # o init não cria o objecto, muda ou acrescenta(costumizador) parametros
        self.cor_das_paredes = cor_das_paredes
        self.cor_do_telhado = cor_do_telhado
        pass

    
# print (House.area)
# print (House.abre_porta) 
# as funçoes da class só podem ser acedidas por objectos
# da classe a menos que se consiga consiga passar um objecto


# exemplo de inicializador
# int()
# float() # estas são funçoes que se chamam inicializadores


concrete_house = House(0xffffff, 0x4682B4) # - 0x indica que o numero é hexadecimal
concrete_house.area = 450
print (concrete_house.cor_das_paredes)
print (concrete_house.cor_do_telhado)
# 16777215 == 0xffffff na consola

another_house = House()
another_house.portas = 2
print ('Quantas portas?' + str(another_house.portas))


# podemos sempre criar outros atributos para a classe fora da classe ou __init__
# isto é uma funcionalidade do python, noutras linguagem nao acontece isso


# -----------------------------------------------
# -- Class com herança --------------------------
# -----------------------------------------------


class Pais:
    
    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade
        
class Eu(Pais):
    
    def __init__(self, nome, idade, hobby):
        super().__init__(nome, idade) 
        # print ( podemos adicionar mais elementos )
        # o super é para ir buscar os elementos que o pai já faz
        self.hobby = hobby
   
        
eu = Eu('Andreia Faria', 31, 'Ler')
print ('O meu nome é:', str(eu.nome))
print ('A minha idade é:', str(eu.idade))
print ('O meu hobby é:', str(eu.hobby))


# -----------------------------------------------