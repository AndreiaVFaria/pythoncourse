# -----------------------------------------------
# --------- Andreia Faria ----- Session 4 -------
# -----------------------------------------------
# -- 29.10.2020 ---------------------------------
# -----------------------------------------------
# -- Working with files -------------------------
# -----------------------------------------------



# f = open('blobfile.txt', 'w')
# write - escreve info no ficheiro, mas apaga o conteudo original

# f = open('blobfile.txt', 'a')
# append - adiciona ao final do ficheiro sem apagar a info previa

# f = open('new.txt', 'x')
# uma forma de cria um ficheiro e dá erro se o fich já existir

f = open('blobfile.txt', 'r')
# abre-se um ficheiro para leitura
# print (f.read())
# read_result = f.read(40)
# lê os primeiros 40 caracteres, e o espaço em branco conta como 1 ch
# read_result = f.readline() # lê uma linha
# print (read_result)
print (f.readline())

for line in f: # semelhante a fazer f.readline()
    print (line)

# no final do trabalho temos que fechar o ficheiro
f.close()


# with open('blobfile.txt') as file_reader:# forma para nao ter de escrever open e close
#     for line in file_reader: 
#         print (line)

print (f.closed) # dá para verificar se o ficheiro está aberto ou não


f = open('blobfile.txt', 'a')
f.write('Agora tem mais info.')
f.close()

f = open('blobfile.txt', 'r')
print (f.read())


# -----------------------------------------------
# -- import modulo os ---------------------------
# -----------------------------------------------


import os
os.path.abspath('blobfile.txt') # mostra o caminho absoluto
os.path.relpath('blobfile.txt') # mostra o caminho relativo
os.path.basename('./blobfile.txt') #./ - quer dizer diretoria actual, dá o nome do ficheiro

# os.remove('blobfile.txt') # remove o ficheiro indicado


# -----------------------------------------------
# -- abrir / editar / fechar ficheiro -----------
# -----------------------------------------------


# se quisermos mudificar o texto dá jeito ter em lista 
lines = []
lines_for_writing = []
lines_after_writing = []

import copy

with open('blobfile.txt') as file_reader:
    lines = file_reader.readlines() # lê uma lista de strings
    lines_for_writing = copy.deepcopy(lines)
    for line in lines:
        lines_for_writing = line.replace('s','p')
    print (lines_for_writing)

with open('blobfile.txt', 'w') as file_writer:
    file_writer.writelines(lines)
    
with open('blobfile.txt') as file_reader:
    lines_after_writing = file_reader.readlines()
    print(lines_after_writing == lines)


# está com erros


# -----------------------------------------------
# -- tratamento de execções ---------------------
# -----------------------------------------------


int('abc')
# ValueError: invalid literal for int() with base 10: 'abc'

try:
    int('abs')
except ValueError:
    print ('the string must contais numbers not letters')
finally: # falhe ou não o finally é sempre executado
    print ('thnx')


try:
    a = int('123')
    a = a // 0
except ValueError:
    print ('the string must contais numbers not letters')
except:
    print ('something went wrong')
finally: # falhe ou não o finally é sempre executado
    print ('thnx')
    
with open('blobfile.txt') as file_reader:
    try:
        file_reader.readlines()
        # do something
    except expression as identifier: 
        identifier
        # handle
# no final o ficheiro fica fechado por default



f = open('blobfile.txt')
try:
    # do something with the file
except expression as identifier:
    # handle expression
finally:
    f.close()
# neste caso é necessário fechar
    

# -----------------------------------------------
# -----------------------------------------------


# -----------------------------------------------
# -----------------------------------------------


# -----------------------------------------------
# -----------------------------------------------









