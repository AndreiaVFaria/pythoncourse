# -----------------------------------------------
# -- Andreia Faria ------------------------------
# -----------------------------------------------
# -- Exercicios Ficha -- 1 ----------------------
# -----------------------------------------------
# -- variáveis, Fluxo de controlo e Loops -------
# -----------------------------------------------

# --------------------- ex 1


f = float(input('Please enter temperature:'))
c = (f - 32) // 1.8
print (c)


# --------------------- ex 2


yearB = int(input('Please enter your year of birth:'))
print ('Your age a year from now is:', 2020 - yearB)


# --------------------- ex 3


h = '01:28:42'
h_comp = h.split(':')
seg_total = int(h_comp[0])*3600 + int(h_comp[1])*60 + int(h_comp [2])
print (seg_total)


# --------------------- ex 4 


# hora = 3600 seg
# minuto = 60 seg
ttl_seg = 9999
ttl_hour = ttl_seg // 3600
ttl_min = (ttl_seg-(ttl_hour*3600)) // 60
ttl_segu = ttl_seg - (ttl_hour*3600 + ttl_min*60)

print (ttl_hour, ':', ttl_min, ':', ttl_segu)


# --------------------- ex 5


x = 1
if x == 1:
    x = x + 1   # x = 2
    if x == 1:
        x = x + 1
    else:
        x = x - 1
else:
    x = x - 1   # x = 1


# --------------------- a) 
'''
o valor final é 1, porque o else dentro do primeiro if 
retira 1 unidade ao valor de x. 
'''
# --------------------- b)
'''
o valor teria de ser 0
'''
# --------------------- c)
'''
o segundo if nunca é executado porque, como tem a mesma condição
do if anterior e no if anterior existe uma transformação do x 
passa logo para o else
'''


# --------------------- ex 6


roman_num = { 1:'I', 2:'II', 3:'III', 4:'IV', 5:'V'}
num = int(input('Por favor, escolha um número de 1 a 5:\n'))

print(roman_num.get(num)) 


# --------------------- ex 7


'''
escrever um prog que leia um ano (>0) e escreva
o século a que pertence

'''

ano = int(input('Escreva um ano:'))
temp = ano // 100
resto = ano % 100
print (temp if resto == 0 else temp +1)


# --------------------- ex 8


'''
escrever um prog que recebe ano, mes, dia
em separado, e um numero de dias x, e 
devolva nova data x dias mais tarde
ano bisexto se é divisivel por 4 

'''

ano = int(input("ano: "))
mes = int(input("mês: "))
dia = int(input("dia: "))
dias_add = int(input("que dia será daqui a este número de dias? "))

meses_dias = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31, 8: 31, 9: 30, 10: 31, 11: 30, 12:31}

e_ano_bisexto = (mes == 2 and ano % 4 == 0 and not (ano % 400 == 0 and ano % 100 == 0))
dias_do_mes = meses_dias[mes] + e_ano_bisexto 
# you can add boolean expressions to numbers, as if True it will be interpreted as one and if False it will be interpreted as 0

while dia + dias_add > dias_do_mes:
    ano += (mes + 1) > 12 
    # é adicionado um se o proximo mês for menor que 12 (Dezembro)
    mes = (mes + 1) % 12 if mes + 1 > 12 else mes + 1 
    # se o mês for maior que doze incrementamos o mês o obtemos o resto da divisão (ex: mês = 13 então mês % 12 = 1)
    dias_add -= dias_do_mes - dia 
    # removemos os restantes dias que faltam no mês dos dias adicionados
    dia = 0 
    # pomos o dia como 0 para adicionar-mos os restantes dias adicionais caso saia do loop ou no próximo loop sem ter um dia a mais
    dias_do_mes = meses_dias[mes] + e_ano_bisexto 
    # atualizamos os dias do mês

dia += dias_add 
# adicionamos os restantes dias do mês

print(f"{ano}-{mes}-{dia}")


# --------------------- ex 9


x = 1
y = 4
if x==1 and y<5:
    y=y+2
print (y)


# --------------------- ex 10


'''
o prog não termina porque i+1 deve ser i+=1 (i = i+1)
'''

n = int(input('Escreva um núremo inteiro:'))
print ('Tabuada do', n, ':')
i = 1
while i <= 10:
    print (n, 'x', i, '=', n * i)
    i += 1

'''
Escreva um núremo inteiro:4
Tabuada do 4 :
4 x 1 = 4
4 x 2 = 8
4 x 3 = 12
4 x 4 = 16
4 x 5 = 20
4 x 6 = 24
4 x 7 = 28
4 x 8 = 32
4 x 9 = 36
4 x 10 = 40
'''


# --------------------- ex 11

# --------------------- a)
'''
O programa apresenta o número de vezes que é possivel aplicar 
o divisor ao dividendo mostrando o resto, até não ser possível 
dividir mais os número inteiros.
'''
# --------------------- b) 
'''
As operações em python que implementam a mesma funcionalidade 
é : // - indica o quociente
e : % - indica o resto
'''
# --------------------- c) 


while True:
    dividendo = int(input('Dividendo:'))
    divisor = int(input('Divisor:'))
    resto = dividendo
    quociente = 0
    if dividendo < divisor:
        print ('Por favor insira um divisor menor que o dividendo')
        continue
    else resto >= divisor:
        resto = resto - divisor
        quociente = quociente + 1
        print ('O quociente é', quociente, 'e o resto é', resto)
        break


# --------------------- d)


while True:
    dividendo = int(input('Dividendo:'))
    divisor = int(input('Divisor:'))
    resto = dividendo
    quociente = 0
    if dividendo <= 0 and divisor <= 0:
        print ('O dividendo ou divisor têm de ser positivos')
        continue
    elif dividendo < divisor:
        print ('Por favor insira um divisor menor que o dividendo')
        continue
    else:
        resto = resto - divisor
        quociente = quociente + 1
        print ('O quociente é', quociente, 'e o resto é', resto)
        break


# --------------------- ex 12


# --------------------- ex 13

'''
Escreva um programa em Python que peça ao utilizador 
um número decimal e escreva no ecrã a sua parte inteira 
perguntando em seguida se o utilizador quer terminar a utilizaçãodo programa.
'''
 
def fun_int():
    num_dec = float(input('Please insert a decimal number:\n'))
    cal = num_dec // 1
    num_int = int(cal)
    return print ('Your whole number is:', num_int)


while True:
    fun_int()
    answer = input('Do you want to close the program? y or n\n')
    if answer == 'n':
        continue
    elif answer == 'y':
        break
    else:
        print("Error. Try again.")
        continue


# --------------------- ex 14 


# --------------------- ex 15


num = int(input('Please insert a whole number:'))
ttl = 1
for i in range (0, num +1):
    ttl = ttl*3**i

print(ttl)


# --------------------- ex 16
'''
Escreva um programa em Python que que peça ao utilizador 
um número inteiro k maior do que 2 e escreva no ecrã 
quantos números perfeitos existem entre 2 e k (inclusive).
Por exemplo, existem 4 números perfeitos 
entre 2 e 10000 (o 6, o 28, o 496 e o 8128)
'''

def perfect_number(n):
    sum = 0
    for x in range(1, n):
        if n % x == 0:
            sum += x
    return sum == n

# print(perfect_number(k))

while True:
    k = int(input('Please insert a whole number greater than 2:'))
    if k > 2:
        for i in range (2, k + 1):
            if perfect_number(i):
                print (i)
        break
    else:
        print ('Please insert a number greater than 2.')


# --------------------- ex 17


counter=0
k=int(input('Insira um número maior que 10:'))

if k<10:
    print('Tem de ser um número válido.')
else:
    for c in range(10,k+1):
        c=str(c)
        if c==c[::-1]:
            # compara o num com o seu reverso
            counter+=1 
            # aumenta um valor ao counter
        else:
            continue
print('Existem {} capícuas entre 10 e {}.'.format(counter, k))

