# -----------------------------------------------
# -- Andreia Faria ------------------------------
# -----------------------------------------------
# -- Exercicios Ficha -- 3 ----------------------
# -----------------------------------------------
# -- Funcoes alto nivel -------------------------
# -----------------------------------------------

# --------------------- ex 1 

# lambda é uma função que só existe no momento e depois é descartada, equivale a uma def

a = list(map(lambda x:x+1,range(1,4)))
# adiciona +1 a cada elemento do range
# por isso é que o 4 saí em output
# [2, 3, 4]

b = list(map(lambda x:x>0,[3, −5, −2, 0]))

c = list(filter(lambda x:x>5,range(1,7)))
# acabar por ser um loop for / if analisa o range
# e retorna os que são maiores de 5,
# neste caso retorna só o 6 porque o último elemento do range é ignorado

d = list(filter(lambda x:x%2==0,range(1,11)))
# retorna os números pares de 1 a 10

# --------------------- ex 2 


import functools
a = functools.reduce(lambda y, z: y* 3+z, range(1,5)) 
# 58
b = functools.reduce (lambda x, y: x** 2+y, range(2,6)) 
# 2814

