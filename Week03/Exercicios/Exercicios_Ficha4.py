# -----------------------------------------------
# -- Andreia Faria ------------------------------
# -----------------------------------------------
# -- Exercicios Ficha -- 4 ----------------------
# -----------------------------------------------
# -- Ficheiros e tratamento de excepções --------
# -----------------------------------------------
# --------------------- ex 1


# --------------------- ex 2


with open('___.txt') as file_reader:
    for line in file_reader:
        print(line)
        

# --------------------- ex 3 

line_n = 0
with open('___.txt') as file_reader:
    for line in file_reader:
        line_n += 1
        print(line_n, line)
        

# --------------------- ex 4 


from statistics import mean

def medias(ficheiro):
    file_reader = open(ficheiro)
    try:
        with open(ficheiro) as file_reader:
            for line in file_reader:
                line = line.strip()
                split_line = line.split()
                # for i in range(len(split_line)):
                #     split_line[i] = float(split_line[i])
                float_list = list(map(float, split_line))
                print (round(mean(float_list),2))
    except:
        print('Something went wrong reading the file')            
               
medias('C:\\Users\\Huga\\Desktop\\PythonCourse\\Week03\\temperaturas.txt')


# --------------------- ex 5 


def ficheiro_erro():
    user_input = input('Indique o nome do ficheiro:')
    try:
        medias(user_input)
    except:
        print('Erro de I/O ao ler o ficheiro')


# --------------------- ex 6 


def linha_para_elemento(line):
    line = line.split()
    dict1 = {'nome':line[0],'número atómico':line[1],'densidade':line[2]}
    return dict1
with open('C:\\Users\\Huga\\Desktop\\PythonCourse\\Week03\\Exercicios\\elementos.txt', 'r', encoding ='utf-8') as elementos:
    lista = []    
    for line in elementos:
        lista.append(linha_para_elemento(line))
print(lista)


# --------------------- ex 7 


def escrever_elementos(nome_ficheiro,lista):
    with open(nome_ficheiro,'w') as ficheiro:
        for elemento in lista:
            ficheiro.write(elemento['nome']+' ')
            ficheiro.write(str(elemento['número atómico'])+' ')
            ficheiro.write(str(elemento['densidade'])+'\n')
nome_ficheiro = input('Insira um nome para o ficheiro:\n')
nome_ficheiro = nome_ficheiro + '.txt'
escrever_elementos(nome_ficheiro,lista)


# --------------------- ex 8 

   

# -----------------------------------------------
# -- Manipulação de CSV -------------------------
# -----------------------------------------------

# --------------------- ex 1 


import csv
with open('eggs.csv', newline='') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    for row in spamreader:
        print(', '.join(row))
    #print (spamreader.__next__()) #lê uma linha de cada vez


# --------------------- ex 2 


import csv
def grafico_para_csv_linha(nome_ficheiro,grafico):
    with open(nome_ficheiro, 'w', newline='') as ficheiro_csv:
        escritor = csv.writer(ficheiro_csv)
        escritor.writerow(grafico[0])
        escritor.writerow(grafico[1])
nome_ficheiro = input('Indique um nome para o ficheiro: ')
nome_ficheiro = nome_ficheiro+'.csv'
grafico = [[1,3,5,7],[2,4,6,8]]
grafico_para_csv_linha(nome_ficheiro,grafico)


# --------------------- ex 3 


