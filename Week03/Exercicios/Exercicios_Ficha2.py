# -----------------------------------------------
# -- Andreia Faria ------------------------------
# -----------------------------------------------
# -- Exercicios Ficha -- 2 ----------------------
# -----------------------------------------------
# -- Funcoes Simples ----------------------------
# -----------------------------------------------

# --------------------- ex 1
# --------------------- a)
'''
A função faz uma divisão, para um valor inteiro, dos valores dados.
'''
# --------------------- b)


imprimeDivisaoInteira(4, 2)
# chama a função para fazer a divisão inteira de 4 por 2 = 2 (quociente)
imprimeDivisaoInteira(2, 4)
# chama a função para fazer a divisão inteira de 2 por 4 = 0
# o resultado é zero, porque esta divisão "real" é um número decimal
imprimeDivisaoInteira(3, 0)
# imprime 'Divisão por zero' porque não é possivel fazer uma divisão inteira por zero
help(imprimeDivisaoInteira)
# mostra que inputs a função necessita de ter, docstring
imprimeDivisaoInteira()
# chama a função


# --------------------- c)


def imprimeDivisaoInteira(x,y):
    '''

    Parameters
    ----------
    x : int 
        Dividendo maior que 0.
    y : int
        Divisor maior que 0.

    Returns (x//y)
    -------
    None.

    '''
    return (x//y)


# --------------------- ex 2 
# --------------------- a) 

a=2
b=3
potencia(b,a)
# chama a função, 3 elevado a 2 = 3**2
potencia(a,b)
# chama a função, 2 elevado a 3 = 2**3
print(potencia(b,a))
# imprime o resultado da função 3 elevado a 2 = 3**2
print(potencia(a,b))
# imprime o resultado da função 2 elevado a 3 = 2**3
print(potencia(2,0))
# imprime o resultado da função 2 elevado a 0 = 2**0
print(potencia(2))
# precisa de um segundo elemento para funcionar


# --------------------- b) 


def potenciaP (a):
    return a**a


# --------------------- c)


def potencia (a, b = None):
    return a**a if b == None else a**b

a = 2
b = None

print (potencia(a,b))

 
# --------------------- ex 3
# --------------------- a) 
'''
Dentro da função: 17
Fora da função: 4
'''
# --------------------- b) 
'''
A variável global é 4, porque se encontra fora da função.
A variável local é 17, porque se encontra dentro da função.
'''


# --------------------- ex 4 
# --------------------- a)
'''
deve ser um número inteiro e positivo
'''
# --------------------- b)
'''
Obtemos a soma dos divisores de um número dado.
'''
# --------------------- ex 5 


def SomaDivisores(num):
    divisors = [1]
    for i in range(2, num):
        if (num % i)==0:
            # se o resto for igual a 0
            divisors.append(i)
    return sum(divisors)

while True:
    num = int(input("Escreva um número inteiro positivo:"))
    print('A soma dos divisores é: ', SomaDivisores(num))
    if num <= -1:
        print("O número deve ser positivo.")
        break


# --------------------- ex 6


import datetime
t = datetime.datetime.now()
dia_actual = t.day
mes_actual = t.month
ano_actual = t.year

def data_parents(parent):
    if parent == "Pai":
        print("Dados do Pai")
    else:
        print("Dados da Mãe")
    ano=int(input("Introduza o ano do nascimento: "))
    mes=int(input("Introduza o mês do nascimento: "))
    dia=int(input("Introduza o dia do nascimento: "))
    return (ano, mes, dia, parent)

def anos_atuais(ano, mes, dia, parent):
    if mes > mes_actual or ( mes == mes_actual and dia > dia_actual):
        print(parent,"tem",ano_actual - ano -1,"anos")
    else:
        print(parent,"tem",ano_actual - ano,"anos")

dadosPai = data_parents("Pai")
dadosMae = data_parents("Mãe")

anos_atuais(dadosPai[0],dadosPai[1],dadosPai[2],dadosPai[3])
anos_atuais(dadosMae[0],dadosMae[1],dadosMae[2],dadosMae[3])
    
    
# --------------------- ex 7


num1 = int(input('Insira um número inteiro:'))
num2 = int(input('Insira outro número inteiro:'))

def maximum (a, b):
    '''
    Devolve o maior dos dois números
    ----------
    a : INT
        um número inteiro para comparar
    b : INT
        outro número inteiro

    Returns
    -------
    INT
        o maior dos dois números

    '''
    return a if a > b else b

print (maximum(num1, num2))


def minimum(a, b):
    return b if a == maximum(num1, num2) else a

print (minimum(num1, num2))


# --------------------- ex 8


def int_corte (a):
    a = str(a)
    if len(a) < 2:
        return 0
    else:
        return int(a[0:-1])
print (int_corte(467))
print (int_corte(9))


# -- outra solução


def int_corte (a):
    return a // 10
print (int_corte(467))
print (int_corte(9))


# --------------------- ex 9


def plus0 (a):
    '''
    Parameters
    ----------
    a : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    '''
    return a * 10
n = int(input('Insert a whole number:'))      
print (plus0(n))


# --------------------- ex 10 

# o dividor próprio é o próprio número

def SomaDivisores(num):
    divisors = [1]
    for i in range(2, num):
        if (num % i)==0:
            # se o resto for igual a 0
            divisors.append(i)
    return sum(divisors)

while True:
    num = int(input("Escreva um número inteiro positivo:"))
    print('A soma dos divisores é: ', SomaDivisores(num))
    if num <= -1:
        print("O número deve ser positivo.")
        break


# --------------------- ex 11

# cousin number é só divisivel por 1 e por ele

def cousin(a):
    for i in range(3, n):
        if n % i == 0:
            return print("This isn't a prime number.")
    return print('The number is prime.')

n = int(input('Is it or not a prime number?\nPlease enter a whole number:\n'))
print (cousin(n))




# --------------------- ex 12

